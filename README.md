# LOGIN

## **Requerimientos Funcionales**

### **Inicio de sesión**

Se tomará el nombre de usuario (username) o correo electrónico. La contraseña deberá ser formateada en Base64. No se requieren cifrados adicionales puesto que el canal SSL de la conexión puede garantizar la seguridad.
El endpoint de salida devolverá un token JWT con los datos más importantes para el usuario.

- Versión
- Audiencia
- Emisor
- Fecha de emisión
- Fecha de caducidad
- Fecha de validación
- JTI (Id. de token) para realizar tareas de actualización del token de acceso (similar a OpenIDConnect).
- subject (sujeto de autenticación de importancia de negocio, es decir, datos abreviados de usuario)

En caso de que el inicio de sesión no sea correcto, se devolverá un código de estatus 401 (Unauthorized).
No se devuelven causas del fallo por motivos de seguridad.

Aparecerá una casilla de verificación que indicará si se recuerda la contraseña.

### **Alta de usuarios**

Se consideran los campos

- Correo electrónico. Validar formato. El requerimiento del negocio no menciona que sea obligatorio, pero se propone obligatorio por estandar.
- Nombre de usuario. Obligatorio. Longitud mínima de 7 caracteres.
- Contraseña. Obligatorio. Se forzará a considerar al menos una letra mayúscula, minúscula, símbolo y dígito. Mayor a 10 caracteres. El endpoint sólo lo recibirá formateado como Base64. Esto se hace debido a que representa más riesgo mantener claves de cifrado o vectoes de inicialización en las aplicaciones cliente. Sin embargo, en base de datos se cifrará por el algoritmo BCrypt. Ver <https://en.wikipedia.org/wiki/Bcrypt>.
- Estatus. Valor boleano. True implica un usuario activo. False un usuario no activo.
- Sexo. La API recibirá "F"/"M" . Se usan estos valores para mantener compatibilidad con el idioma inglés (Male, Female). Será responsabilidad de las aplicaciones cliente traducirlo a Masculino/Femenino.
- La API añadirá fecha de creación internamente.

### **Actualizar usuarios**

Este componente se encargará de mostrar los datos del usuario y ofrecerá la posibilidad de editar:

- Detalles de usuario: nombre de usuario, contraseña, género.
- Contraseña: cambiar contraseña, incluir la confirmación.
- Añadir o eliminar roles.

Este componente será invocado en dos escenarios: Perfil del usuario y editar usuario desde las búsquedas.

### **Búsqueda de usuarios**

Este componente se encargará de ejecutar búsquedas de usuarios registrados, por nombre de usuario o correo electrońico, además de filtrar por usuario activo e inactivo.

Además de estas funcionalidades, para cada usuario encontrado se podrán realizar las tareas:

- Editar el estatus del usuario (hablitar o deshabilitarlo).
- Ver sus datos además de la posibilidad de modificarlos.

Se podrá exportar a formato Excel la rejilla de usuarios encontrados en la búsqueda.

### Roles y perfiles

El sistema estará conformado por autoridades y facultades. Una autoridad le otorga al usuario un rol que le permitirá ejercer distintas facultades.

| Rol                | Manager | Supervisor | User |
|--------------------|---------|------------|------|
| Registrar usuarios |    X    |     X      |      |
| Actualizar datos   |    X    |     X      |   X  |
| Modificar roles    |    X    |            |      |
| Buscar usuarios    |    X    |     X      |      |

## **Requerimientos no funcionales**

La aplicación deberá ser desarrollada por dós módulos:

- FrontEnd: Interfaz de usuario.
- BackEnd: Reglas de negocio.

Además, las contraseñas deben ser seguras por lo que serán cifradas por el algoritmo Hash asimétrico BCrypt de tipo 2B.

La aplicación será capaz de servir los 24h*365d, con capacidad de resiliencia y persistencia agnóstica al motor de base de datos.

La aplicación deberá ser ejecutada en Windows y GNU/Linux

Por cuestión de tiempos, se considera una segunda etapa:

- Inclusión de un motor de seguimiento y bitácoras.
- Mejoras de seguridad a través de un intercepciones de código.
- Tratamiento robusto de excepciones.
- Entrega y despliegue contínuo hacia en cluster de K8S o alguna otra tecnología de contenedores.

### **Configuración**

Para el control de versiones:

- GIT

El repositorio se encuentra activo en la rama *master* de gitlab: <https://gitlab.com/obersd/login.git>.

Para clonar el código fuente, bastará ejecutar en el equipo cliente:

```bash
git clone https://gitlab.com/obersd/login.git
```

Estructura del proyecto

El proyecto se divice en las carpetas:

- front. Código fuente del front de usuario.
- back. Código fuente de la API.
- docs. Documentación del proyecto.
- db. Documentación y scripts de inicialización del proyecto.

Para el FrontEnd, se usaron las siguientes tecnologías:

- Angular 11
- HTML5
- CSS3
- Bootstrap
- OpenIconic
- Javascript
- NPM

Para ejecutar el código, desde la terminal se debe situar en la carpeta front y ejecutar

```bash
npm install
```

Y posteriormente ejecutar:

```bash
ng serve
```

En la carpeta ***/front/src/environments*** residen los archivos environment que contienen la URL que usará el front para invocar endpoints de la API. Por default apunta a ***<https://localhost:5001/api/>***.

Para el BackEnd, se usaron las siguientes tecnologías:

- DotNet 5
- Entity Framework Core
- Kestrel
- Swagger (OpenAPI)

Para ejecutar el código, se requiere tener instalado el SDK y Runtime de .NET 5. Este se descarga desde <https://dotnet.microsoft.com/download>.

Las configuraciones para la aplicación están en el archivo: ***/back/LoginApi/LoginApi/appsettings.Development.json***, estas configuraciones son:

- JwtKey: Palabra de seguridad que se usará para cifrar el token JWT por el algoritmo HS256.
- MinutesToExpire: En cuántos minutos expira el token JWT.
- MinuteGraceToRefreshToken: Periodo de gracia en minutos para que el JWT pueda ser intercambiado (usando el JTI).
- ConnectionStrings:Db: Cadena de conexión de la base de datos.

Para la compilación, desde la terminal debe ubicarse en ***/back/LoginApi*** y ejecutar las sentencia:

```bash
dotnet clean -c Release
dotnet build -c Release
```

Y para ejecución, situarse en la carpeta ***/back/LoginApi/LoginApi/bin/Release/net5.0*** e introducir:

```bash
dotnet LoginApi.dll --environment=Development
```

La aplicación expone un endpoint con la documentación completa de la API a través de: ***<https://localhost:5001/swagger/index.html>***

Para el motor de base de datos, se usó la tecnología:

- Microsoft SQL Server 2019.

Para efectos de velocidad en el desarrollo, se usó Docker para ejeutar una imagen con SQL Server. (Ver <https://docs.microsoft.com/en-us/sql/linux/quickstart-install-connect-docker?view=sql-server-ver15&pivots=cs1-bash>). Esto se ejecuta como:

```bash
sudo docker pull mcr.microsoft.com/mssql/server:2019-latest
sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=R12345678.A" -p 1433:1433 --name sql -h sql -d mcr.microsoft.com/mssql/server:2019-latest
```

Y para verificar la imagen, se puede ejecutar:

```bash
docker ps -a
```

Una vez levantado el servidor, se pueden ejecutar los scripts de inicialización ubicados en ***/db***.

El comando para regenerar el contexto de datos es:

```bash
dotnet ef dbcontext scaffold "Data Source=localhost;Database=LoginDb;User Id=sa;Password=R12345678.A;" "Microsoft.EntityFrameworkCore.SqlServer" -f -c LoginContext --context-dir Contexts -o ../Entities/DataModel -v --no-build
```

### **Documentación**

La documentación de diagramas de clase se encuentra en ***/docs/***. El diagrama de la base de datos se encuentra en ***/db***. Los diagramas de secuencia se encuentran en ***/back/LoginApi/uml***. Se usó la herramienta PlantUML para crear dichos diagramas, las URL para visualizarlos son:

- Login (<http://www.plantuml.com/plantuml/uml/dP91Jm8n48Nl_HMz2Gd8VGyck1ebyO2Wtaixi8tTTfrEL_VVsnP5O0a9Rj9aTwVVlFUclQg86bzC9eqHba9jhgMh9nu-0bW9OwN51aNDH21dGxq2pAz2acu50NUMbKQ39Jw-uTD0LUaieJC6iCZViQIb8Eti8lNqn0vt9Mxsv3RQJfjc2TXg2GL_07hrW5FVMScEomcvMwKMy4K5Vmw07HE6U8W8juxusWMh68zFdptXyxvkg4jw6Jds5u5jn8BFR2kCLYM2Ihc4yGoCXwrm5kMvnSvTfllnTbgdejsMs3LHx0rGhxl1e_3LYC_HjLg1cWllXnbDWRML3X4ayIR-_tJue__Fv9UdJqIUGAxiN5ncugn-7D_UDNB9SmojuCMzWnqaTFYdTh3pTE0CgqoVq-mTsLAqi80rWg-sqlkTHeS--lr-HzYJXtXFu_e6>)
- Autorización (<http://www.plantuml.com/plantuml/uml/XPBBJiCm44Nt_WhlIAM6xBD0WX14aH1HMx6jXdYWLfrni2TfuUjnse10YilIutlkF2-spAtFiwmrGDmrvhAsJURnlKEgCLKQtXmq0Zgsr3Klw4HXD1BBz4fUhwhDdJQCRbFf5eqcpELoG3sBYSyZq_7MEls9ZrefWtjmWLbGRtUO59OO3pmVBMPYGXprA0_ieEPx18Kk6ItcW_GKYhezxFaMNw4pV8CXvCe-jETp-KJhK1GzWz4A69MO5WuMYy4wF7GDR5qk2uU1g9ocMhTWagFHsDrC_5NBz4mDFnPdaiVO5VJuFnyR9_WFgkKQ_6vwRh7r_-QcBw0arLfD75_xjoAUPOc-jUHnEaUWJaVw1G00>)
- Creación de token (<http://www.plantuml.com/plantuml/uml/ZP9DRi8m48NtFeLtXKN8tWkaY5PLKJSDvG36cH8BP-pQOoYtnqu4AWqjtLZ-UU-RcIS_j4GkYB9qHYB5pioKxSe0dn5GGT5ekVMoOpAInTXjmFTRgKZl9G6to1P60n8lvhoAr2uiahV6W1VytUu0wumAnAuVi_mbNoMhLfAi5pN8fdTCAAzJTltEYt65vO5wnotugzrgh9nRWTzh1O8_0wq3-2eSKKtEpjcITAhqGvm1ZviCX2yIRn7yaLtEVIDZV0qV7a8xzFCtSEsQbDrtmtdaEqNe5_WucPWclH_v4ohRmD11TlmdSuJ3xSoJTFWA5ya9>)
- Agregar usuario (<http://www.plantuml.com/plantuml/uml/TL6x3i8m4Dlp5TbeXzAz0rA11J4WyThJz0GHwQKa5nv_JrFn9cmNd-spdC6Ug7L5dhTQ8Fb63wLfSWT73oWXgvNOMT4muScWRogm_IWagPCWu0RPH2j0ujc8RnnODp58rcWDjk1hSm1S1Yz7x6iR1A_NM3WejaAhkdDDtdKf-sJnB79fCO_PptgiRDiLs9EIK9Hr7Q3INL4clfjIzis9wXx10lvN5nC6RlJ4j0fHuK8XqztoYSMz4CwRk5saa0VvfvNGUlzXNMSt>)
- Obtener roles (<http://www.plantuml.com/plantuml/uml/VP4nJyCm48Lt_mflj4F8dW4fQXS6XWRu0LVtr5hOTyO-LuHVJzn8LGiEc-ttlgTxvuUJI4XTsmO79DcxHyE-JVYPaGms1ml725v15gRiznWlHp1Ypo2ecTJ6MIJHpPD-JnZJXaaYEuUnqs_yWJIKh2Jgbrg0vpxBYQFz1h5CVGYl6C_MODSxrwUHZ5fotD8o3XWuMU4uNidLMjNqmcttPGhyacwNCOxgUjVDKkeqCgQ5mFzwt7D_IrIMDYErPRr029F1GrXTJ5F3-QcQuujq6Effat_-jblR3m00>)

### **Probar usuarios**

Para todos los usuarios se tiene la misma contraseña: Aa.12345

- alex.vargas
- ruben.vargas
- nancy.vargas
- user.master
