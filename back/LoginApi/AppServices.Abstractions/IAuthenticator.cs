// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthenticator.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Abstractions
{
    using System;
    using System.Security.Claims;
    using Microsoft.IdentityModel.Tokens;

    /// <summary>
    /// Exposes functionalities for reading, transforming, encoding and decoding JWT tokens.
    /// </summary>
    public interface IAuthenticator
    {
        /// <summary>
        /// Encodes a JWT token. 
        /// </summary>
        /// <param name="serializationFunc">Serialization function.</param>
        /// <returns>JWT Token.</returns>
        string EncodeToken(Func<dynamic> serializationFunc);

        /// <summary>
        /// Reads a JWT token and returns a SecurityToken.
        /// </summary>
        /// <param name="token">Token.</param>
        /// <returns>Security Token.</returns>
        SecurityToken ReadToken(string token);

        /// <summary>
        /// Creates a principal object.
        /// </summary>
        /// <param name="securityToken">Security Token.</param>
        /// <returns>Claims Identity.</returns>
        ClaimsPrincipal CreatePrincipal(SecurityToken securityToken);
    }
}