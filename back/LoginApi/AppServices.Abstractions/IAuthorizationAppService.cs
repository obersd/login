// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthorizationAppService.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Abstractions
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities.DataModel;

    /// <summary>
    /// Exposes functionality for role management.
    /// </summary>
    public interface IAuthorizationAppService
    {
        /// <summary>
        /// Adds a new authority to the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="abbreviation">Authority abbreviation.</param>
        /// <returns>Task.</returns>
        Task AddAuthorityAsync(int userId, string abbreviation);
        
        /// <summary>
        /// Gets all roles and their faculties.
        /// </summary>
        /// <returns>Roles and faculties.</returns>
        Task<IEnumerable<Role>> AllAsync();

        /// <summary>
        /// Removes an existing authority from the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="abbreviation">Authority abbreviation.</param>
        /// <returns>Task.</returns>
        Task RemoveAuthorityAsync(int userId, string abbreviation);
    }
}