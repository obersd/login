namespace AppServices.Abstractions
{
    using System.Threading.Tasks;
    using Entities.DataModel;

    /// <summary>
    /// Login application services.
    /// </summary>
    public interface ILoginAppService
    {
        /// <summary>
        /// Gets user by password and username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password in string base64</param>
        /// <returns>User.</returns>
        Task<User> GetUserAsync(string username, string password);

        /// <summary>
        /// Gets user who has stored a valid access token.
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <returns>User.</returns>
        Task<User> GetUserAsync(string refreshToken);

        /// <summary>
        /// Saves a user refresh token. 
        /// </summary>
        /// <param name="refreshToken">Refresh token.</param>
        /// <param name="userId">User id.</param>
        /// <returns>Task.</returns>
        Task SaveRefreshTokenAsync(string refreshToken, int userId);
    }
}