// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IPasswordChecker.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices.Abstractions
{
    public interface IPasswordChecker
    {
        /// <summary>
        /// Check password.
        /// </summary>
        /// <param name="password">password</param>
        /// <param name="confirmation">confirmation.</param>
        /// <returns>true if password is ok.</returns>
        (bool Success, string Message) Check(string password, string confirmation);
    }
}