namespace AppServices.Abstractions
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Entities.DataModel;
    using Entities.Model;

    /// <summary>
    /// Exposes functionalities for user management.
    /// </summary>
    public interface IUsersAppService
    {
        /// <summary>
        /// Add user into system.
        /// </summary>
        /// <param name="user">User to add.</param>
        /// <returns>User added and error messages generated by the operation.</returns>
        Task<(User User, string Message)> AddUserAsync(User user);

        /// <summary>
        /// Update a user email, username and gender.
        /// </summary>
        /// <param name="user">User to update.</param>
        /// <returns>Error messages generated by the operation.</returns>
        Task<string[]> UpdateUserAsync(UserPatch user);

        /// <summary>
        /// Get an user by user id.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>User.</returns>
        Task<User> GetUser(int userId);

        /// <summary>
        /// Updates user password.
        /// </summary>
        /// <param name="user">User.</param>
        /// <returns>Task.</returns>
        Task UpdatePasswordAsync(User user);

        /// <summary>
        /// Finds users.
        /// </summary>
        /// <param name="status">user Status.</param>
        /// <param name="keyword">Term search.</param>
        /// <returns></returns>
        Task<IEnumerable<User>> FindAsync(bool status, string keyword);

        /// <summary>
        /// Changes the user status.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="status">new status.</param>
        /// <returns></returns>
        Task ChangeUserStatusAsync(int userId, bool status);
    }
}