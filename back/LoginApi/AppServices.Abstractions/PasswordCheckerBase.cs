namespace AppServices.Abstractions
{
    public abstract class PasswordCheckerBase : IPasswordChecker
    {
        /// <inheritdoc />
        public abstract (bool Success, string Message) Check(string password, string confirmation);
        
        /// <summary>
        /// Min length.
        /// </summary>
        public virtual int MinLength { get; set; }
        
        /// <summary>
        /// Confirmation not equal message.
        /// </summary>
        public virtual string ConfirmationNotEqualMessage { get; set; }
        
        /// <summary>
        /// Password not in base64 format message.
        /// </summary>
        public virtual string PasswordNotInBase64Message { get; set; }
        
        /// <summary>
        /// Pattern for match.
        /// </summary>
        public virtual string Pattern { get; set; }
        
        /// <summary>
        /// Passord not match with pattern message.
        /// </summary>
        public virtual string PasswordNotMatchWithPatternMessage { get; set; }
        
        /// <summary>
        /// Passwod is less than limite message.
        /// </summary>
        public virtual string PasswordIsLessThanLimitMessage { get; set; }
        
        /// <summary>
        /// Passwod required message.
        /// </summary>
        public virtual string PasswordRequiredMessage { get; set; }
    }
}