namespace AppServices
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using Abstractions;
    using Entities.Model.Options;
    using Entities.Model.Security;
    using Microsoft.Extensions.Options;
    using Microsoft.IdentityModel.Tokens;

    /// <inheritdoc />
    public class Authenticator : IAuthenticator
    {
        public const string AuthenticationDefaultValue = "LoginTest";
        private readonly JwtSecurityTokenHandler jwtSecurityTokenHandler;
        private readonly long minutesToExpiration;
        private readonly SecurityKey securityKey;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="jwtSecurityTokenHandler">Token handler.</param>
        /// <param name="options">Options.</param>
        public Authenticator(JwtSecurityTokenHandler jwtSecurityTokenHandler,
            IOptionsSnapshot<AuthenticatorOptions> options)
        {
            this.jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            var opts = options.Get(nameof(Authenticator));
            this.minutesToExpiration = opts.MinutesToExpire;
            this.securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(opts.JwtKey));
        }

        /// <inheritdoc />
        public string EncodeToken(Func<dynamic> serializationFunc)
        {
            var payload = new JwtPayload
            {
                {"iat", DateTimeOffset.UtcNow.ToUnixTimeSeconds()},
                {"nbf", DateTimeOffset.UtcNow.ToUnixTimeSeconds()},
                {"exp", DateTimeOffset.UtcNow.AddMinutes(this.minutesToExpiration).ToUnixTimeSeconds()},
                {"jti", $"{Guid.NewGuid():N}{Guid.NewGuid():N}"},
                {"sub", serializationFunc()},
                {"iss", AuthenticationDefaultValue},
                {"aud", AuthenticationDefaultValue},
                {"ver", "1.1"}
            };

            return this.jwtSecurityTokenHandler.WriteToken(new JwtSecurityToken(
                new JwtHeader(new SigningCredentials(this.securityKey, SecurityAlgorithms.HmacSha256)), payload));
        }

        /// <inheritdoc />
        public SecurityToken ReadToken(string token) => this.jwtSecurityTokenHandler.ReadJwtToken(token);

        /// <inheritdoc />
        public ClaimsPrincipal CreatePrincipal(SecurityToken securityToken) =>
            new(this.CreateIdentity((JwtSecurityToken) securityToken));

        /// <summary>
        /// Creates an identity object.
        /// </summary>
        /// <param name="securityToken">Security Token.</param>
        /// <returns>Claims Identity.</returns>
        private ClaimsIdentity CreateIdentity(SecurityToken securityToken)
        {
            var jwtToken = (JwtSecurityToken) securityToken;
            var user = UserIdentity.CreateFromSubject(jwtToken.Subject);
            var identity = new ClaimsIdentity(AuthenticationDefaultValue, ClaimTypes.Name, ClaimTypes.Role)
            {
                BootstrapContext = new IdentityBootstrapContext(user)
            };

            var claims = new List<Claim>
            {
                new(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new(ClaimTypes.Name, user.Username ?? string.Empty),
                new(ClaimTypes.Email, user.Email ?? string.Empty),
            };

            claims.AddRange(user.Authorities.Select(item => new Claim(ClaimTypes.Role, item.Abbreviation)));
            identity.AddClaims(claims);
            return identity;
        }
    }
}