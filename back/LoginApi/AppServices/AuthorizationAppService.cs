// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAuthorizationAppService.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using A2net.DataAccess.EfCore;
    using Abstractions;
    using DataRepositories.Abstractions;
    using Entities.DataModel;

    /// <summary>
    /// Exposes functionality for role management.
    /// </summary>
    public class AuthorizationAppService : IAuthorizationAppService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly Lazy<IAuthorityRepository> authorityRepository;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="unitOfWork">Unit of work.</param>
        public AuthorizationAppService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            this.authorityRepository = new Lazy<IAuthorityRepository>(() => this.unitOfWork.Get<IAuthorityRepository>(),
                LazyThreadSafetyMode.ExecutionAndPublication);
        }

        /// <inheritdoc />
        public async Task AddAuthorityAsync(int userId, string abbreviation)
        {
            if (await this.authorityRepository.Value.CreateUserAuthorityAsync(userId, abbreviation))
            {
                await this.unitOfWork.SaveAsync();
            }
        }

        public async Task<IEnumerable<Role>> AllAsync() => await this.authorityRepository.Value.AllAsync();

        /// <inheritdoc />
        public async Task RemoveAuthorityAsync(int userId, string abbreviation)
        {
            if (!(await this.authorityRepository.Value.GetUserRolesAsync(userId)).Any())
            {
                return;
            }

            if (await this.authorityRepository.Value.RemoveUserAuthorityAsync(userId, abbreviation))
            {
                await this.unitOfWork.SaveAsync();
            }
        }
    }
}