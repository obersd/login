﻿namespace AppServices
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using Abstractions;
    using BCrypt.Net;
    using DataRepositories.Contexts;
    using Entities.DataModel;
    using Entities.Model.Options;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Options;

    /// <inheritdoc />
    public class LoginAppService : ILoginAppService
    {
        private readonly LoginContext ctx;
        private readonly double minuteGraceToRefreshToken;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        /// <param name="options">Optons.</param>
        public LoginAppService(LoginContext ctx, IOptionsSnapshot<LoginOptions> options)
        {
            this.ctx = ctx;
            this.minuteGraceToRefreshToken = options.Get(nameof(LoginAppService)).MinuteGraceToRefreshToken;
        }

        /// <inheritdoc />
        public async Task<User> GetUserAsync(string username, string password)
        {
            var user = await this.ctx.Users.Include(x => x.Userroles).ThenInclude(x => x.Role)
                .FirstOrDefaultAsync(x => x.Username == username || x.Email == username);
            if (user == null)
            {
                return User.EmptyUser;
            }

            if (!BCrypt.Verify(Encoding.UTF8.GetString(Convert.FromBase64String(password)), user.Pwd))
            {
                return User.EmptyUser;
            }

            this.ctx.Entry(user).State = EntityState.Detached;
            user.Pwd = string.Empty;
            return user;
        }

        /// <inheritdoc />
        public async Task<User> GetUserAsync(string refreshToken)
        {
            var token = await this.ctx.Refreshtokens.Include(x => x.User)
                .ThenInclude(x => x.Userroles).ThenInclude(x => x.Role)
                .FirstOrDefaultAsync(x => x.Isactive && x.Token == refreshToken);
            if (token == null)
            {
                return User.EmptyUser;
            }

            var diff = DateTime.Now - token.Creationdate;
            if (diff.TotalMinutes > this.minuteGraceToRefreshToken)
            {
                return User.EmptyUser;
            }

            token.Isactive = false;
            this.ctx.Refreshtokens.Update(token);
            await this.ctx.SaveChangesAsync();
            this.ctx.Entry(token).State = EntityState.Detached;
            token.User.Pwd = string.Empty;
            return token.User;
        }

        /// <inheritdoc />
        public async Task SaveRefreshTokenAsync(string refreshToken, int userId)
        {
            await this.ctx.Refreshtokens.AddAsync(new Refreshtoken
            {
                Creationdate = DateTime.Now,
                Isactive = true,
                Token = refreshToken,
                UserId = userId
            });
            await this.ctx.SaveChangesAsync();
        }
    }
}