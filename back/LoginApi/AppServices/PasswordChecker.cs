namespace AppServices
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;
    using Abstractions;

    /// <summary>
    /// Password verifications. Invalid conditions:
    ///  - password or confirmation is null or whitespace.
    ///  - password and confirmation are not equals.
    ///  - Not in string base 64 format.
    ///  - password must contains at least one upper char, lower char, digit and symbol.
    /// </summary>
    public sealed class PasswordChecker : PasswordCheckerBase
    {
        /// <summary>
        /// Check password.
        /// </summary>
        /// <param name="password">password</param>
        /// <param name="confirmation">confirmation.</param>
        /// <returns>true if password is ok.</returns>
        public override (bool Success, string Message) Check(string password, string confirmation)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                return (false, this.PasswordRequiredMessage);
            }

            if (!string.Equals(password, confirmation))
            {
                return (false, this.ConfirmationNotEqualMessage);
            }

            string decodedPassword;
            try
            {
                decodedPassword = Encoding.UTF8.GetString(Convert.FromBase64String(password));
            }
            catch
            {
                return (false, this.PasswordNotInBase64Message);
            }

            if (decodedPassword.Length < this.MinLength)
            {
                return (false, this.PasswordIsLessThanLimitMessage);
            }

            return Regex.IsMatch(decodedPassword, this.Pattern)
                ? (true, string.Empty)
                : (false, this.PasswordNotMatchWithPatternMessage);
        }
    }
}