// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddUser.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace AppServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Abstractions;
    using BCrypt.Net;
    using DataRepositories.Contexts;
    using Entities.DataModel;
    using Entities.Model;
    using Microsoft.EntityFrameworkCore;

    /// <inheritdoc />
    public class UsersAppService : IUsersAppService
    {
        private readonly LoginContext ctx;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="ctx">Database context.</param>
        public UsersAppService(LoginContext ctx)
        {
            this.ctx = ctx;
        }

        /// <inheritdoc />
        public async Task<(User User, string Message)> AddUserAsync(User user)
        {
            var existentUser =
                await this.ctx.Users.FirstOrDefaultAsync(x => x.Username == user.Username || x.Email == user.Email);
            if (existentUser != null)
            {
                return (User.EmptyUser, "El usuario ya existe.");
            }

            foreach (var roleItem in user.Userroles)
            {
                var role = await this.ctx.Roles.FirstOrDefaultAsync(x => x.Abbreviation == roleItem.Role.Abbreviation);
                if (role == null)
                {
                    continue;
                }

                roleItem.Role = role;
                roleItem.User = user;
            }

            user.Pwd = BCrypt.HashString(Encoding.UTF8.GetString(Convert.FromBase64String(user.Pwd)));
            await this.ctx.Users.AddAsync(user);
            await this.ctx.SaveChangesAsync();
            this.ctx.Entry(user).State = EntityState.Detached;
            user.Pwd = string.Empty;
            return (user, string.Empty);
        }

        /// <inheritdoc />
        public async Task<string[]> UpdateUserAsync(UserPatch user)
        {
            var existentUser = await this.ctx.Users.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            if (existentUser == null)
            {
                return new[] {"El usuario no existe."};
            }

            var update = false;
            var result = new List<string>();
            if (user.Email != null)
            {
                if (user.Email.Data != existentUser.Email)
                {
                    if (await this.ctx.Users.AnyAsync(x => x.Email == user.Email.Data))
                    {
                        result.Add("El correo electrónico ya existe.");
                    }
                    else
                    {
                        existentUser.Email = user.Email.Data;
                        update = true;
                    }
                }
            }

            if (user.Gender != null)
            {
                existentUser.Gender = user.Gender.Data;
                update = true;
            }

            if (user.Username != null)
            {
                if (user.Username.Data != existentUser.Username)
                {
                    if (await this.ctx.Users.AnyAsync(x => x.Username == user.Username.Data))
                    {
                        result.Add("El nombre de usuario ya existe.");
                    }
                    else
                    {
                        update = true;
                        existentUser.Username = user.Username.Data;
                    }
                }
            }

            if (update)
            {
                existentUser.Modificationdate = DateTime.Now;
                this.ctx.Users.Update(existentUser);
                await this.ctx.SaveChangesAsync();
            }

            return result.ToArray();
        }

        /// <inheritdoc />
        public async Task<User> GetUser(int userId) => await this.ctx.Users.Include(x => x.Userroles)
            .ThenInclude(y => y.Role).FirstOrDefaultAsync(x => x.UserId == userId);

        /// <inheritdoc />
        public async Task UpdatePasswordAsync(User user)
        {
            var existentUser = await this.ctx.Users.FirstOrDefaultAsync(x => x.UserId == user.UserId);
            if (existentUser == null)
            {
                return;
            }

            existentUser.Pwd = BCrypt.HashString(Encoding.UTF8.GetString(Convert.FromBase64String(user.Pwd)));
            this.ctx.Users.Update(existentUser);
            await this.ctx.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<User>> FindAsync(bool status, string keyword)
        {
            return await this.ctx.Users.Include(x => x.Userroles).ThenInclude(y => y.Role).Where(z =>
                z.Status == status && (EF.Functions.Like(z.Email, $"{keyword}%") ||
                                       EF.Functions.Like(z.Username, $"{keyword}%"))).Select(w =>
                new User
                {
                    Creationdate = w.Creationdate,
                    Email = w.Email,
                    Gender = w.Gender,
                    Modificationdate = w.Modificationdate,
                    Status = w.Status,
                    Username = w.Username,
                    Userroles = w.Userroles,
                    UserId = w.UserId
                }).ToListAsync();
        }

        /// <inheritdoc />
        public async Task ChangeUserStatusAsync(int userId, bool status)
        {
            var user = await this.ctx.Users.FindAsync(userId);
            if (user != null)
            {
                user.Status = status;
                this.ctx.Users.Update(user);
                await this.ctx.SaveChangesAsync();
            }
        }
    }
}