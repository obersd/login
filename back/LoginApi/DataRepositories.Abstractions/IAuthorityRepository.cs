﻿using System;

namespace DataRepositories.Abstractions
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using A2net.DataAccess.EfCore;
    using Entities.DataModel;

    /// <summary>
    /// Authorities data persistence operations.
    /// </summary>
    public interface IAuthorityRepository : IRepository
    {
        /// <summary>
        /// Adds a new authority to the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="abbreviation">Authority abbreviation.</param>
        /// <returns>Task.</returns>
        Task<bool> CreateUserAuthorityAsync(int userId, string abbreviation);

        /// <summary>
        /// Gets the user authorities.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>Authorities.</returns>
        Task<IEnumerable<Role>> GetUserRolesAsync(int userId);

        /// <summary>
        /// Gets all roles and their faculties.
        /// </summary>
        /// <returns>Roles and faculties.</returns>
        Task<IEnumerable<Role>> AllAsync();

        /// <summary>
        /// Removes an existing authority from the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="abbreviation">Authority abbreviation.</param>
        /// <returns>Task.</returns>
        Task<bool> RemoveUserAuthorityAsync(int userId, string abbreviation);
    }
}