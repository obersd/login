﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DataRepositories;

#nullable disable

namespace DataRepositories.Contexts
{
    using Entities.DataModel;

    public partial class LoginContext : DbContext
    {
        public LoginContext()
        {
        }

        public LoginContext(DbContextOptions<LoginContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Faculty> Faculties { get; set; }
        public virtual DbSet<Refreshtoken> Refreshtokens { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Rolefaculty> Rolefaculties { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Userrole> Userroles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Faculty>(entity =>
            {
                entity.ToTable("faculties");

                entity.Property(e => e.FacultyId).HasColumnName("facultyId");

                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("abbreviation");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Refreshtoken>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.Token })
                    .HasName("PK__refresht__6733115843477A28");

                entity.ToTable("refreshtokens");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Token)
                    .HasMaxLength(128)
                    .HasColumnName("token");

                entity.Property(e => e.Creationdate)
                    .HasColumnType("datetime")
                    .HasColumnName("creationdate");

                entity.Property(e => e.Isactive).HasColumnName("isactive");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Refreshtokens)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("refreshtokens_userid_fk");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.Abbreviation)
                    .IsRequired()
                    .HasMaxLength(16)
                    .HasColumnName("abbreviation");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(128)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Rolefaculty>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.FacultyId })
                    .HasName("PK__rolefacu__C023B0D1B0C788AC");

                entity.ToTable("rolefaculties");

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.Property(e => e.FacultyId).HasColumnName("facultyId");

                entity.HasOne(d => d.Faculty)
                    .WithMany(p => p.Rolefaculties)
                    .HasForeignKey(d => d.FacultyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("rolefaculties_facultyId_fk");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Rolefaculties)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("rolefaculties_roleId_fk");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.Creationdate)
                    .HasColumnType("datetime")
                    .HasColumnName("creationdate");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasColumnName("email");

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasMaxLength(1)
                    .HasColumnName("gender")
                    .IsFixedLength(true);

                entity.Property(e => e.Modificationdate)
                    .HasColumnType("datetime")
                    .HasColumnName("modificationdate");

                entity.Property(e => e.Pwd)
                    .IsRequired()
                    .HasMaxLength(512)
                    .HasColumnName("pwd");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("username");
            });

            modelBuilder.Entity<Userrole>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PK__userrole__7743989DCF6F3CBE");

                entity.ToTable("userroles");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.Property(e => e.RoleId).HasColumnName("roleId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Userroles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userroles_roleid_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userroles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userroles_userid_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
