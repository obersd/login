// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthorityRepository.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace DataRepositories.RepositoryPattern
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Abstractions;
    using Entities.DataModel;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Authorities data persistence operations with repository pattern.
    /// </summary>
    public class AuthorityRepository : Repository, IAuthorityRepository
    {
        /// <inheritdoc />
        public async Task<bool> CreateUserAuthorityAsync(int userId, string abbreviation)
        {
            var userRole = await this.Context.Userroles.Include(x => x.Role).FirstOrDefaultAsync(x => x.UserId == userId
                && x.Role.Abbreviation == abbreviation);
            if (userRole != null)
            {
                return false;
            }

            var role = await this.Context.Roles.FirstOrDefaultAsync(x => x.Abbreviation == abbreviation);
            if (role == null)
            {
                return false;
            }

            await this.Context.Userroles.AddAsync(new Userrole
            {
                UserId = userId,
                Role = role
            });
            return true;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Role>> GetUserRolesAsync(int userId)
        {
            return await this.Context.Userroles.Include(x => x.Role).Where(x => x.UserId == userId).Select(x => x.Role)
                .ToListAsync();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<Role>> AllAsync()
        {
            return await this.Context.Roles.Include(x => x.Rolefaculties).ThenInclude(x => x.Faculty).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<bool> RemoveUserAuthorityAsync(int userId, string abbreviation)
        {
            var userRole = await this.Context.Userroles.Include(x => x.Role).FirstOrDefaultAsync(x => x.UserId == userId
                && x.Role.Abbreviation == abbreviation);
            if (userRole == null)
            {
                return false;
            }

            this.Context.Userroles.Remove(userRole);
            return true;
        }
    }
}