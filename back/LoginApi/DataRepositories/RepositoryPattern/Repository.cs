namespace DataRepositories.RepositoryPattern
{
    using A2net.DataAccess.EfCore;
    using Contexts;

    public abstract class Repository : IRepository
    {
        protected LoginContext Context => this.UoW.Context;
        private UnitOfWork UoW => (UnitOfWork) this.UnitOfWork; 
        public IUnitOfWork UnitOfWork { get; set; }
    }
}