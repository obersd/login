namespace DataRepositories.RepositoryPattern
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using A2net.DataAccess.EfCore;
    using A2net.DependencyInjection.Resolver;
    using Contexts;

    /// <inheritdoc />
    public sealed class UnitOfWork : IUnitOfWork
    {
        public LoginContext Context { get; }
        private readonly ResolverService resolverService;
        private readonly IDictionary<string, IRepository> repoInstances;

        public UnitOfWork(LoginContext context, ResolverService resolverService)
        {
            this.repoInstances = new Dictionary<string, IRepository>();
            this.Context = context;
            this.resolverService = resolverService;
        }

        /// <inheritdoc />
        public TRepository Get<TRepository>() where TRepository : IRepository
        {
            var key = typeof(TRepository).FullName;
            if (this.repoInstances.TryGetValue(key, out var repo))
            {
                repo.UnitOfWork = this;
                return (TRepository) repo;
            }

            var repoEx = this.resolverService.GetService<TRepository>(key) as IRepository;
            repoEx.UnitOfWork = this;
            this.repoInstances.Add(key, repoEx);
            return (TRepository) repoEx;
        }

        /// <inheritdoc />
        public async Task SaveAsync() => await this.Context.SaveChangesAsync();

        public void Rollback() => this.Context.Database.RollbackTransaction();

        /// <inheritdoc />
        public async Task BeginTransactionAsync() => await this.Context.Database.BeginTransactionAsync();

        /// <inheritdoc />
        public async Task<TTransaction> BeginTransactionAsync<TTransaction>() => (TTransaction) await this.Context.Database.BeginTransactionAsync();

        /// <inheritdoc />
        public void Commit() => this.Context.Database.CommitTransaction();

        /// <inheritdoc />
        public async void Dispose()
        {
            if (this.Context != null)
            {
                await this.Context.DisposeAsync();
            }
        }
    }
}