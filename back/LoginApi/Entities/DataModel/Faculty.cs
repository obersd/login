﻿#nullable disable

namespace Entities.DataModel
{
    using System.Collections.Generic;

    public partial class Faculty
    {
        public Faculty()
        {
            this.Rolefaculties = new HashSet<Rolefaculty>();
        }

        public int FacultyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }

        public virtual ICollection<Rolefaculty> Rolefaculties { get; set; }
    }
}
