﻿#nullable disable

namespace Entities.DataModel
{
    using System;

    public partial class Refreshtoken
    {
        public int UserId { get; set; }
        public string Token { get; set; }
        public DateTime Creationdate { get; set; }
        public bool Isactive { get; set; }

        public virtual User User { get; set; }
    }
}
