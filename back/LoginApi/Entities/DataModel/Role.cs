﻿#nullable disable

namespace Entities.DataModel
{
    using System.Collections.Generic;

    public partial class Role
    {
        public Role()
        {
            this.Rolefaculties = new HashSet<Rolefaculty>();
            this.Userroles = new HashSet<Userrole>();
        }

            public int RoleId { get; set; }
            public string Name { get; set; }
            public string Abbreviation { get; set; }

        public virtual ICollection<Rolefaculty> Rolefaculties { get; set; }
        public virtual ICollection<Userrole> Userroles { get; set; }
    }
}
