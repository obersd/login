﻿#nullable disable

namespace Entities.DataModel
{
    public partial class Rolefaculty
    {
        public int RoleId { get; set; }
        public int FacultyId { get; set; }

        public virtual Faculty Faculty { get; set; }
        public virtual Role Role { get; set; }
    }
}
