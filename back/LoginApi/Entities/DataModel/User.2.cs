﻿namespace Entities.DataModel
{
    using System.Linq;
    using Model.Security;

    public partial class User
    {
        public static readonly User EmptyUser = null;

        /// <summary>
        /// Converts User to UserIdentity. Used by Authenticator to serialize User.
        /// </summary>
        /// <returns>User identity.</returns>
        public UserIdentity ToUserIdentity()
        {
            var user = new UserIdentity(this.UserId, this.Username, this.Email, string.Empty);
            foreach (var item in this.Userroles.Where(x => x.Role != null && x.RoleId > 0 && x.Role.RoleId > 0))
            {
                user.AddAuthority(new UserAuthority(item.RoleId, item.Role?.Name, item.Role?.Abbreviation));
            }

            return user;
        }
    }
}