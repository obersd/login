﻿#nullable disable

namespace Entities.DataModel
{
    using System;
    using System.Collections.Generic;

    public partial class User
    {
        public User()
        {
            this.Refreshtokens = new HashSet<Refreshtoken>();
            this.Userroles = new HashSet<Userrole>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public bool Status { get; set; }
        public string Pwd { get; set; }
        public DateTime Creationdate { get; set; }
        public DateTime? Modificationdate { get; set; }

        public virtual ICollection<Refreshtoken> Refreshtokens { get; set; }
        public virtual ICollection<Userrole> Userroles { get; set; }
    }
}
