// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AuthenticatorOptions.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Entities.Model.Options
{
    /// <summary>
    /// Authentication options.
    /// </summary>
    public sealed class AuthenticatorOptions
    {
        /// <summary>
        /// Jwt key to sign jwt tokens.
        /// </summary>
        public string JwtKey { get; set; }
        
        /// <summary>
        /// Minutes to expire token.
        /// </summary>
        public long MinutesToExpire { get; set; }
    }
}