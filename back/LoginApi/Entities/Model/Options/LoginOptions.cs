// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginConfigurations.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Entities.Model.Options
{
    /// <summary>
    /// Login configurations.
    /// </summary>
    public sealed class LoginOptions
    {
        /// <summary>
        /// Grace period to refresh token. 
        /// </summary>
        public double MinuteGraceToRefreshToken { get; set; }
    }
}