namespace Entities.Model
{
    /// <summary>
    /// Entity for Patch operations.
    /// </summary>
    public abstract class Patch
    {
        /// <summary>
        /// Data object.
        /// </summary>
        public abstract object DataObject { get; }
    }

    /// <summary>
    /// Entity for patch operations.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Patch<T> : Patch where T : class
    {
        /// <summary>
        /// Data.
        /// </summary>
        public T Data { get; set; }
        
        /// <summary>
        /// Get data object.
        /// </summary>
        public override object DataObject => this.Data;
    }
}