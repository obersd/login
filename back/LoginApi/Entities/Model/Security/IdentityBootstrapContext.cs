// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IdentityBootstrapContext.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Entities.Model.Security
{
    /// <summary>
    /// Source object used to construct principal object.
    /// </summary>
    public class IdentityBootstrapContext
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="userIdentity">user Identity.</param>
        public IdentityBootstrapContext(UserIdentity userIdentity)
        {
            this.UserIdentity = userIdentity;
        }

        /// <summary>
        /// User identity.
        /// </summary>
        public UserIdentity UserIdentity { get; }
    }
}