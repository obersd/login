// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Policies.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Entities.Model.Security
{
    /// <summary>
    /// Contains policies, constraints and permissions required by authorization and CORS.
    /// </summary>
    public static class Policies
    {
        /// <summary>
        /// Cors for allow all origins (not for production).
        /// </summary>
        public const string CorsAllowOrigins = "AllowAllOrigins";
    }
}