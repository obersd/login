namespace Entities.Model.Security
{
    /// <summary>
    /// The user's authority corresponds to a role defined in the business model.
    /// </summary>
    public class UserAuthority
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="authorityId">Id.</param>
        /// <param name="name">name.</param>
        /// <param name="abbreviation">Abbreviation.</param>
        public UserAuthority(long authorityId, string name, string abbreviation)
        {
            this.AuthorityId = authorityId;
            this.Name = name;
            this.Abbreviation = abbreviation;
        }

        /// <summary>
        /// Id.
        /// </summary>
        public long AuthorityId { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Abbreviation.
        /// </summary>
        public string Abbreviation { get; set; }
    }
}