// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserIdentity.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Entities.Model.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Json;

    /// <summary>
    /// User identity. This object is used by the Universal Identity infrastructure. It allows to obtain information
    /// about the authenticated user by a request to the API. 
    /// </summary>
    public class UserIdentity
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="userId">Id.</param>
        /// <param name="username">username.</param>
        /// <param name="email">Email.</param>
        /// <param name="token">token (if exists).</param>
        public UserIdentity(long userId, string username, string email, string token)
        {
            this.UserId = userId;
            this.Username = username;
            this.Email = email;
            this.Authorities = new List<UserAuthority>();
            this.Token = token;
        }

        /// <summary>
        /// User id.
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        public string Username { get; }

        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// User authorities.
        /// </summary>
        public IList<UserAuthority> Authorities { get; }

        /// <summary>
        /// Token (if exists).
        /// </summary>
        public string Token { get; }

        /// <summary>
        /// Adds an authority to user authorities list.
        /// </summary>
        /// <param name="userAuthority">Authority.</param>
        public void AddAuthority(UserAuthority userAuthority)
        {
            this.Authorities.Add(userAuthority);
        }

        /// <summary>
        /// Serialize this user to Json format. Uses the dynamic type to represent it.
        /// </summary>
        /// <returns>User with json dynamic type repreresentation.</returns>
        public dynamic Serialize() =>
            new
            {
                id = this.UserId,
                name = this.Username,
                mail = this.Email,
                roles = this.Authorities.Select(x => new
                {
                    name = x.Name,
                    abbr = x.Abbreviation
                })
            };

        /// <summary>
        /// Creates an user identity subject property of JWT Token.
        /// </summary>
        /// <param name="subject">Subject.</param>
        /// <returns>User Identity.</returns>
        public static UserIdentity CreateFromSubject(string subject)
        {
            JsonElement userData = JsonSerializer.Deserialize<dynamic>(subject);
            var userIdentity = new UserIdentity(
                userData.TryGetProperty("id", out var userId) ? userId.GetInt64() : 0,
                userData.TryGetProperty("name", out var username) ? username.GetString() : string.Empty,
                userData.TryGetProperty("mail", out var email) ? email.GetString() : string.Empty,
                subject);

            if (!userData.TryGetProperty("roles", out var authorities))
            {
                return userIdentity;
            }

            var authoritiesArray = authorities.EnumerateArray();
            foreach (var item in authoritiesArray.Select(
                x => new UserAuthority(0,
                    x.TryGetProperty("name", out var abbr) ? abbr.GetString() : string.Empty,
                    x.TryGetProperty("abbr", out var name) ? name.GetString() : string.Empty)).ToList())
            {
                userIdentity.AddAuthority(item);
            }

            return userIdentity;
        }
    }
}