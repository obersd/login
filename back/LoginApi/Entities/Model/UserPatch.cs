namespace Entities.Model
{
    /// <summary>
    /// User patch.
    /// </summary>
    public sealed class UserPatch
    {
        /// <summary>
        /// User id.
        /// </summary>
        public long UserId { get; set; }
        
        /// <summary>
        /// Username to modification. If null then not need updates.
        /// </summary>
        public Patch<string> Username { get; set; }
        
        /// <summary>
        /// Email to modification. If null then not need updates.
        /// </summary>
        public Patch<string> Email { get; set; }
        
        /// <summary>
        /// Gender to modification. If null then not need updates.
        /// </summary>
        public Patch<string> Gender { get; set; }
    }
}