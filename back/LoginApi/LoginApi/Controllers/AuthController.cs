using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers
{
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Threading.Tasks;
    using A2net.WebHelpers.Responses;
    using AppServices.Abstractions;
    using BCrypt.Net;
    using Entities.Model.Security;
    using Microsoft.AspNetCore.Cors;
    using Microsoft.AspNetCore.Http;
    using Model.Requests;
    using Model.Responses;

    /// <summary>
    /// This controller handles the login and JWT token issuance.
    /// </summary>
    [ApiController]
    [Route("api/auth")]
    [Produces("application/json")]
    [Consumes("application/json")]
    [EnableCors(Policies.CorsAllowOrigins)]
    public class AuthController : ControllerBase
    {
        private readonly ILoginAppService loginAppService;
        private readonly IAuthenticator authenticator;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="loginAppService">The loging app. service.</param>
        /// <param name="authenticator">The authenticator.</param>
        public AuthController(ILoginAppService loginAppService, IAuthenticator authenticator)
        {
            var salto = BCrypt.GenerateSalt();
            var pwd = BCrypt.HashPassword("Aa.12345");
            this.loginAppService = loginAppService;
            this.authenticator = authenticator;
        }

        /// <summary>
        /// Performs login into the application.
        /// </summary>
        /// <param name="request">Data.</param>
        /// <response code="200">User logged in.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LoginResponse>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            var user = await this.loginAppService.GetUserAsync(request.Username, request.Pwd);
            if (user == Entities.DataModel.User.EmptyUser)
            {
                return this.Unauthorized(null);
            }

            var token = this.authenticator.EncodeToken(() => user.ToUserIdentity().Serialize());
            await this.loginAppService.SaveRefreshTokenAsync(
                ((JwtSecurityToken) this.authenticator.ReadToken(token)).Payload["jti"].ToString(), user.UserId);
            return this.Ok(new LoginResponse(token));
        }

        /// <summary>
        /// Requests a new access token via a refresh token.
        /// </summary>
        /// <param name="request">Data.</param>
        /// <response code="200">Token updated.</response>
        /// <response code="400">Verify validations.</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Route("token")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<LoginResponse>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> Refresh(RefreshTokenRequest request)
        {
            var user = await this.loginAppService.GetUserAsync(
                ((JwtSecurityToken) this.authenticator.ReadToken(request.Token)).Payload["jti"].ToString());
           
            if (user != Entities.DataModel.User.EmptyUser)
            {
                return this.Ok(
                    new LoginResponse(this.authenticator.EncodeToken(() => user.ToUserIdentity().Serialize())));
            }

            return this.Unauthorized(null);
        }
    }
}