using Microsoft.AspNetCore.Mvc;

namespace LoginApi.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using A2net.WebHelpers.Responses;
    using AppServices.Abstractions;
    using AutoMapper;
    using Entities.DataModel;
    using Entities.Model;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Model.Requests;
    using Model.Responses;

    /// <summary>
    /// This controller handles the user administration.
    /// </summary>
    [ApiController]
    [Route("api/users")]
    [Produces("application/json")]
    [Consumes("application/json")]
    public class UsersController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IUsersAppService usersAppService;
        private readonly IAuthorizationAppService authorizationAppService;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="mapper">Mapper service.</param>
        /// <param name="usersAppService">The Users app service.</param>
        /// <param name="authorizationAppService">The authorization app service.</param>
        public UsersController(IMapper mapper, IUsersAppService usersAppService,
            IAuthorizationAppService authorizationAppService)
        {
            this.mapper = mapper;
            this.authorizationAppService = authorizationAppService;
            this.usersAppService = usersAppService;
        }

        /// <summary>
        /// Get an user by user id.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <response code="200">User retrieved.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <response code="204">User not found.</response>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Authorize]
        [Route("user/{uid}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(UserResponse))]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> GetUser([FromRoute(Name = "uid")] int userId)
        {
            var user = await this.usersAppService.GetUser(userId);
            if (user != null)
            {
                return this.Ok(this.mapper.Map<UserResponse>(user));
            }

            return this.NoContent();
        }

        /// <summary>
        /// Adds new user to application.
        /// </summary>
        /// <param name="request">Data</param>
        /// <response code="200">User added.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Authorize]
        [Route("user")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> AddUser(AddUserRequest request)
        {
            var result = await this.usersAppService.AddUserAsync(this.mapper.Map<User>(request));
            if (result.User != Entities.DataModel.User.EmptyUser)
            {
                return this.Ok();
            }

            return this.BadRequest(ResponseFactory.CreateApiResponse(errors: new List<string>() {result.Message}));
        }

        /// <summary>
        /// Changes the user status.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="status">new Status</param>
        /// <response code="200">User status changed.</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>Action Result.</returns>
        [HttpPut]
        [Authorize]
        [Route("user/{uid}/{st}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        public async Task<IActionResult> DeleteUser([FromRoute(Name = "uid")] int userId = 0,
            [FromRoute(Name = "st")] bool status = true)
        {
            await this.usersAppService.ChangeUserStatusAsync(userId, status);
            return this.Ok();
        }

        /// <summary>
        /// Updates the user password.
        /// </summary>
        /// <param name="request">Data</param>
        /// <response code="200">User updated.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPut]
        [Authorize]
        [Route("user")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdatePassword(UpdatePasswordRequest request)
        {
            await this.usersAppService.UpdatePasswordAsync(this.mapper.Map<User>(request));
            return this.Ok();
        }

        /// <summary>
        /// Finds users by status and any coincidence in username or email fields.
        /// </summary>
        /// <param name="status">User status. 1 = Active (true); 0 = Inactive (false)</param>
        /// <param name="word">Searh term.</param>
        /// <response code="200">Results.</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("user")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<UserResponse>))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        public async Task<IActionResult> Search([FromQuery(Name = "st")] bool status = true,
            [FromQuery(Name = "w")] string word = null)
        {
            var users = await this.usersAppService.FindAsync(status,
                string.IsNullOrWhiteSpace(word) ? string.Empty : word);
            return this.Ok(this.mapper.Map<IEnumerable<UserResponse>>(users));
        }

        /// <summary>
        /// Updates user data.
        /// </summary>
        /// <param name="request">Data</param>
        /// <response code="200">User updated.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPatch]
        [Authorize]
        [Route("user")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> UpdateUser(UpdateUserRequest request)
        {
            var messages = await this.usersAppService.UpdateUserAsync(this.mapper.Map<UserPatch>(request));
            if (messages.Any())
            {
                return this.BadRequest(ResponseFactory.CreateApiResponse(errors: messages.ToList()));
            }

            return this.Ok();
        }

        /// <summary>
        /// Adds a new authority to the user.
        /// </summary>
        /// <param name="request">Data</param>
        /// <response code="200">Role added.</response>
        /// <response code="400">Verify validations</response>
        /// <response code="401">Invalid credentials.</response>
        /// <returns>ActionResult.</returns>
        [HttpPost]
        [Authorize]
        [Route("roles")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiResponse))]
        public async Task<IActionResult> AddAuthority(UserAuthorityRequest request)
        {
            await this.authorizationAppService.AddAuthorityAsync(request.UserId, request.Abbreviation);
            return this.Ok();
        }

        /// <summary>
        /// Removes an existing authority from the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <response code="200">Authority removed.</response>
        /// <response code="401">Invalid credentials.</response>
        /// <param name="abbreviation">Role abbreviation.</param>
        /// <returns>ActionResult.</returns>
        [HttpDelete]
        [Authorize]
        [Route("roles")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = null)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = null)]
        public async Task<IActionResult> RemoveAuthority([FromQuery(Name = "uid")] int userId = 0,
            [FromQuery(Name = "abbr")] string abbreviation = "")
        {
            await this.authorizationAppService.RemoveAuthorityAsync(userId, abbreviation);
            return this.Ok();
        }

        /// <summary>
        /// Gets all roles and their faculties.
        /// </summary>
        /// <response code="200">Roles.</response>
        /// <returns>ActionResult.</returns>
        [HttpGet]
        [Route("roles")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<AuthorityRoleResponse>))]
        public async Task<IActionResult> AllAuthorities()
        {
            return this.Ok(
                this.mapper.Map<IEnumerable<AuthorityRoleResponse>>((await this.authorizationAppService.AllAsync())));
        }
    }
}