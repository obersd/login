// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JwtDefaultBearerEvents.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Infrastructure
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Threading;
    using System.Threading.Tasks;
    using AppServices.Abstractions;
    using Microsoft.AspNetCore.Authentication.JwtBearer;

    /// <summary>
    /// This class handles the process of extraction, validation and transformation JWT token into Principal and
    /// Identity objects.
    /// </summary>
    public class JwtDefaultBearerEvents : JwtBearerEvents
    {
        private const string Authorization = "Authorization";
        private const string FailureMessage = "Authentication token is required.";
        private readonly IAuthenticator authenticator;

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="authenticator">The authenticator.</param>
        public JwtDefaultBearerEvents(IAuthenticator authenticator) => this.authenticator = authenticator;

        /// <summary>
        /// This method extracts JWT token from authorization header and sets into the context.
        /// If authorization header not exists then the extraction fails.
        /// If the token is not a "bearer token" then the validation fails.
        /// Otherwhise, the token will be set into the context.
        /// </summary>
        /// <param name="context">Context of the request.</param>
        /// <returns>Task.</returns>
        public override Task MessageReceived(MessageReceivedContext context)
        {
            if (!context.HttpContext.Request.Headers.ContainsKey(Authorization))
            {
                context.Fail(FailureMessage);
                return Task.CompletedTask;
            }

            var requestToken = context.HttpContext.Request.Headers[Authorization][0];
            var bearerPreffix = $"{JwtBearerDefaults.AuthenticationScheme} ";
            if (!requestToken.StartsWith(bearerPreffix, StringComparison.CurrentCultureIgnoreCase))
            {
                context.Fail(FailureMessage);
                return Task.CompletedTask;
            }

            context.Token = requestToken.Substring(bearerPreffix.Length);
            return Task.CompletedTask;
        }

        /// <summary>
        /// This method creates a Principal Identity object with security token and set it into the HttpContext Pricipal.
        /// </summary>
        /// <param name="context">Context of the request.</param>
        /// <returns>Task.</returns>
        public override Task TokenValidated(TokenValidatedContext context)
        {
            var principal = this.authenticator.CreatePrincipal((JwtSecurityToken) context.SecurityToken);
            Thread.CurrentPrincipal = principal;
            context.HttpContext.User = principal;
            context.Principal = principal;
            context.Success();
            return Task.CompletedTask;
        }
    }
}