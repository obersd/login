// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ServiceExtensions.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Text;
    using A2net.DataAccess.EfCore;
    using A2net.DependencyInjection.Resolver;
    using A2net.Swagger.SwaggerService;
    using AppServices;
    using AppServices.Abstractions;
    using DataRepositories.Abstractions;
    using DataRepositories.Contexts;
    using DataRepositories.RepositoryPattern;
    using Entities.Model.Options;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Tokens;
    using Model.MappingProfiles;

    /// <summary>
    /// This extensions is used to perform additional configuration settings. 
    /// </summary>
    internal static class ServiceExtensions
    {
        /// <summary>
        /// Adds application services to an API.
        /// </summary>
        /// <param name="services">Service collection.</param>
        /// <param name="configuration">Configuration object.</param>
        /// <returns>Service collection instance.</returns>
        private static void AddMainServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddAutoMapper(expression => { expression.AddProfile<MappingProfile>(); });
            services.AddScoped<JwtDefaultBearerEvents>();
            services.AddScoped<PasswordCheckerBase, PasswordChecker>();
            services.AddScoped<JwtSecurityTokenHandler>();

            services.AddScoped<IUsersAppService, UsersAppService>();
            services.AddScoped<IAuthorizationAppService, AuthorizationAppService>();
            services.AddScoped<ILoginAppService, LoginAppService>();
            services.Configure<LoginOptions>(nameof(LoginAppService), configuration.GetSection("Secured"));

            services.AddScoped<IAuthenticator, Authenticator>();
            services.Configure<AuthenticatorOptions>(nameof(Authenticator), configuration.GetSection("Secured"));
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ResolverService>();
            services.AddScoped<IAuthorityRepository, AuthorityRepository>(typeof(IAuthorityRepository).FullName);
        }

        /// <summary>
        /// Selects the main identity object from available identities list.
        /// </summary>
        /// <param name="identities">Identities list.</param>
        /// <returns>Claims Identity.</returns>
        private static ClaimsIdentity PrimaryIdentitySelector(IEnumerable<ClaimsIdentity> identities)
        {
            if (identities == null)
            {
                return null;
            }

            var allIdentities = identities.ToList();
            var principalIdentity = allIdentities.FirstOrDefault(x => string.Equals(x.AuthenticationType,
                Authenticator.AuthenticationDefaultValue, StringComparison.OrdinalIgnoreCase));

            if (principalIdentity == null)
            {
                return allIdentities.Any() ? allIdentities.First() : null;
            }

            return principalIdentity;
        }

        /// <summary>
        /// Adds authentication and authorization services.
        /// </summary>
        /// <param name="services">Service collection.</param>
        /// <param name="configuration">Configuration object.</param>
        private static void AddAuthenticationServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            ClaimsPrincipal.PrimaryIdentitySelector = PrimaryIdentitySelector;
            services.AddAuthentication(
                options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultForbidScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultSignOutScheme = JwtBearerDefaults.AuthenticationScheme;
                }).AddJwtBearer(
                JwtBearerDefaults.AuthenticationScheme,
                options =>
                {
                    options.RequireHttpsMetadata = true;
                    options.SaveToken = true;
                    options.IncludeErrorDetails = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Secured:JwtKey"])),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero + TimeSpan.FromMilliseconds(1984),
                        RequireExpirationTime = true
                    };
                    options.EventsType = typeof(JwtDefaultBearerEvents);
                    options.Validate();
                });
        }

        /// <summary>
        /// Adds a set of services for this API.
        /// </summary>
        /// <param name="services">Service collection.</param>
        /// <param name="configuration">Configuration object.</param>
        public static void AddApiServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<LoginContext>(
                builder =>
                {
                    builder.UseSqlServer(configuration.GetConnectionString("Db"));
#if DEBUG
                    builder.EnableSensitiveDataLogging();
#endif
                });

            services.AddMainServices(configuration);
            services.AddSwaggerService(new OpenApiOptions
            {
                Name = "v1",
                ApiDescription = "Documentation API",
                ApiTitle = "Login API",
                ApiVersion = "Alpha",
                ContactEmail = "avargass0200@egresado.ipn.mx",
                ContactName = "Hello!",
                ContactUrl = "https://lexvargas.com",
                XmlDocuumentationPaths = new[] {$"{Assembly.GetExecutingAssembly().GetName().Name}.xml"},
                OperationFilters = new List<KeyValuePair<object[], Type>>
                {
                    new(Array.Empty<object>(), typeof(ContentTypeFilter))
                }
            });
            services.AddAuthenticationServices(configuration);
        }
    }
}