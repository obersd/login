// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TokenRefresherMiddleware.cs" company="lex vargas">
//   lex vargas ® México 2017-2020
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Infrastructure
{
    using System.Security.Claims;
    using System.Threading.Tasks;
    using AppServices.Abstractions;
    using Entities.Model.Security;
    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// This middleware extract access token and redecode a new access token with parametes updated.
    /// </summary>
    public sealed class TokenRefreshMiddleware
    {
        private const string Authorization = "Authorization";

        /// <summary>
        /// The next.
        /// </summary>
        private readonly RequestDelegate next;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenRefreshMiddleware"/> class.
        /// </summary>
        /// <param name="next">
        /// The next.
        /// </param>
        public TokenRefreshMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// The invoke.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="authenticator">
        /// The authenticator.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task Invoke(HttpContext context, IAuthenticator authenticator)
        {
            if (context.Request.Path.StartsWithSegments(("/api")))
            {
                if (context.Request.Headers.ContainsKey(Authorization) &&
                    (context.User?.Identity?.IsAuthenticated ?? false)
                    && context.User?.Identity is ClaimsIdentity
                    {
                        BootstrapContext: IdentityBootstrapContext bootstrapContext
                    })
                {
                    var newToken = authenticator.EncodeToken(() => bootstrapContext.UserIdentity.Serialize());
                    if (context.Response.Headers.ContainsKey(Authorization))
                    {
                        context.Response.Headers[Authorization] = newToken;
                    }
                    else
                    {
                        context.Response.Headers.Add(Authorization, newToken);
                    }
                }
            }

            await this.next(context);
        }
    }
}