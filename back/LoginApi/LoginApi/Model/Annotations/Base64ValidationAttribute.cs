// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Base64ValidationAttribute.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Validates if input is in base64 format.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class Base64ValidationAttribute : ValidationAttribute
    {
        /// <summary>Validates the specified value with respect to the current validation attribute.</summary>
        /// <param name="value">The value to validate.</param>
        /// <param name="validationContext">The context information about the validation operation.</param>
        /// <returns><see cref="ValidationResult.Success"/> if the validation is Ok.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var valueAsString = value?.ToString() ?? string.Empty;
            try
            {
                var _ = Convert.FromBase64String(valueAsString);
            }
            catch 
            {
                return new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});
            }

            return ValidationResult.Success;
        }
    }
}