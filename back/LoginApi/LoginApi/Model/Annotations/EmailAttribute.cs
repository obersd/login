namespace LoginApi.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using A2net.Base.Helpers;

    /// <summary>
    /// Validates if input is in email address format.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class EmailAttribute : ValidationAttribute
    {
        /// <summary>Validates the specified value with respect to the current validation attribute.</summary>
        /// <param name="value">The value to validate.</param>
        /// <param name="validationContext">The context information about the validation operation.</param>
        /// <returns><see cref="ValidationResult.Success"/> if the validation is Ok.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var stringValue = value?.ToString();
            return BasicValidator.Match(stringValue, BasicValidator.EmailPattern)
                ? ValidationResult.Success
                : new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});
        }
    }
}