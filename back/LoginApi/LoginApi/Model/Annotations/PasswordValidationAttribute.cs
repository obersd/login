// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PasswordAttribute.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using AppServices.Abstractions;
    using Microsoft.Extensions.DependencyInjection;
    using Requests;

    /// <summary>
    /// Password validation (requests the PasswordChecker service).
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class PasswordValidationAttribute : ValidationAttribute
    {
        /// <summary>Validates the specified value with respect to the current validation attribute.</summary>
        /// <param name="value">The value to validate.</param>
        /// <param name="validationContext">The context information about the validation operation.</param>
        /// <exception cref="T:System.InvalidOperationException">The current attribute is malformed.</exception>
        /// <returns><see cref="ValidationResult.Success"/> if the validation is Ok.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var passwordChecker = validationContext.GetRequiredService<PasswordCheckerBase>();
            passwordChecker.MinLength = ValidationValues.PasswordMinLengthValue;
            passwordChecker.PasswordNotMatchWithPatternMessage = ValidationValues.PasswordNotMatchWithPatternMessage;
            passwordChecker.Pattern = ValidationValues.PasswordRegex;
            passwordChecker.ConfirmationNotEqualMessage = ValidationValues.PasswordConfirmation;
            passwordChecker.PasswordIsLessThanLimitMessage = ValidationValues.PasswordMinLength;
            passwordChecker.PasswordNotInBase64Message = ValidationValues.Base64Format;
            passwordChecker.PasswordRequiredMessage = ValidationValues.PasswordRequired;

            string pwd, pwdConfirmation;
            switch (value)
            {
                case UpdatePasswordRequest updatePasswordRequest:
                    pwd = updatePasswordRequest.Pwd;
                    pwdConfirmation = updatePasswordRequest.PwdConfirmation;
                    break;
                case AddUserRequest addUserRequest:
                    pwd = addUserRequest.Pwd;
                    pwdConfirmation = addUserRequest.PwdConfirmation;
                    break;
                default:
                    pwd = pwdConfirmation = string.Empty;
                    break;
            }

            var result = passwordChecker.Check(pwd, pwdConfirmation);
            return result.Success
                ? ValidationResult.Success
                : new ValidationResult(result.Message, new[] {"Pwd"});
        }
    }
}