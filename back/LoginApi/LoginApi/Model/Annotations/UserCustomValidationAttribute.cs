namespace LoginApi.Model.Annotations
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using A2net.Base.Helpers;
    using Entities.Model;

    /// <summary>
    /// User custom validation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class UserCustomValidationAttribute : ValidationAttribute
    {
        private readonly DataType dataType;
        private readonly int minValue;
        private readonly string[] validValues;

        /// <summary>
        /// Validation data type.
        /// </summary>
        public enum DataType
        {
            /// <summary>
            /// Validates email data.
            /// </summary>
            Email,

            /// <summary>
            /// Validates gender.
            /// </summary>
            Gender,

            /// <summary>
            /// Validates string length.
            /// </summary>
            String
        }

        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="dataType">DataType to apply validation.</param>
        /// <param name="minValue">Min value if DataTyp is String.</param>
        /// <param name="validValues">Set of valid values if DataType is Gender.</param>
        public UserCustomValidationAttribute(DataType dataType, int minValue, string[] validValues = null)
        {
            this.dataType = dataType;
            this.minValue = minValue;
            this.validValues = validValues;
        }

        /// <summary>Validates the specified value with respect to the current validation attribute.</summary>
        /// <param name="value">The value to validate.</param>
        /// <param name="validationContext">The context information about the validation operation.</param>
        /// <returns><see cref="ValidationResult.Success"/> if the validation is Ok.</returns>
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var request = value as Patch;

            if (request?.DataObject == null)
            {
                return ValidationResult.Success;
            }

            var valueAsString = request.DataObject.ToString() ?? string.Empty;
            if (string.IsNullOrWhiteSpace(valueAsString))
            {
                return new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});
            }

            switch (this.dataType)
            {
                case DataType.Email:
                    var isValid = BasicValidator.Match(valueAsString, BasicValidator.EmailPattern);
                    return isValid
                        ? ValidationResult.Success
                        : new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});

                case DataType.Gender:
                    return this.validValues.Any(
                        x => string.Equals(x, valueAsString, StringComparison.OrdinalIgnoreCase))
                        ? ValidationResult.Success
                        : new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName});

                case DataType.String:
                    return valueAsString.Length < this.minValue
                        ? new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName})
                        : ValidationResult.Success;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}