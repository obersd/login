// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ValidationMessages.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Annotations
{
    /// <summary>
    /// Notable values for validations.
    /// </summary>
    public static class ValidationValues
    {
        /// <summary>
        /// Username required.
        /// </summary>
        public const string UsernameRequired = "El nombre de usuario es requerido.";

        /// <summary>
        /// Authority abbreviation required.
        /// </summary>
        public const string AuthAbbreviation = "El identificador de rol es requerido.";

        /// <summary>
        /// Username min length.
        /// </summary>
        public const string UsernameMinLength = "La longitud debe ser mayor o igual a 7 caracteres.";
        
        /// <summary>
        /// password min length.
        /// </summary>
        public const string PasswordMinLength = "La longitud debe ser mayor o igual a 10 caracteres.";

        /// <summary>
        /// username min length value.
        /// </summary>
        public const int UsernameMinLengthValue = 7;
        
        /// <summary>
        /// password min length value.
        /// </summary>
        public const int PasswordMinLengthValue = 10;

        /// <summary>
        /// Password required.
        /// </summary>
        public const string PasswordRequired = "La contraseña es requerida.";

        /// <summary>
        /// Password regex.
        /// </summary>
        public const string PasswordRegex = @"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)";
        
        /// <summary>
        /// Password confirmation required.
        /// </summary>
        public const string PasswordConfirmationRequired = "La confirmación de la contraseña es requerida.";

        /// <summary>
        /// Base64 format.
        /// </summary>
        public const string Base64Format = "El formato suministrado es incorrecto.";

        /// <summary>
        /// Email format.
        /// </summary>
        public const string EmailFormat = "El formato de correo electrónico es inválido.";

        /// <summary>
        /// Roles required.
        /// </summary>
        public const string RolesRequired = "Se requiere al menos un rol.";

        /// <summary>
        /// Gender required.
        /// </summary>
        public const string GenderRequired = "Valor inválido. F=Femenino, M=Masculino";
        
        /// <summary>
        /// Gender Male or Masculino.
        /// </summary>
        public const string GenderM = "M";
        
        /// <summary>
        /// Gender female or Femenino.
        /// </summary>
        public const string GenderF = "F";

        /// <summary>
        /// Password regex does not match.
        /// </summary>
        public const string PasswordNotMatchWithPatternMessage =
            "Contraseña inválida: debe contener al menos un caracter mayúscula, minúscula, dígito y símbolo.";

        /// <summary>
        /// Password confirmation does not match.
        /// </summary>
        public const string PasswordConfirmation = "La contraseña no coincide con la confirmación.";

        /// <summary>
        /// Token required.
        /// </summary>
        public const string TokenRequired = "El token de actualización es requerido.";
    }
}