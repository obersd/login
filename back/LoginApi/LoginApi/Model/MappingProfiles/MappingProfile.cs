// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MappingProfile.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.MappingProfiles
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Entities.DataModel;
    using Entities.Model;
    using Requests;
    using Responses;

    /// <summary>
    /// Adds mapping functionality for DTOs and Data Model Entities.
    /// </summary>
    public sealed class MappingProfile : Profile
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        public MappingProfile()
        {
            this.CreateMap<Rolefaculty, FacultyResponse>()
                .ForMember(dest => dest.Description,
                    opt => opt.MapFrom((rolefaculty, response) => rolefaculty.Faculty.Description))
                .ForMember(dest => dest.Name, opt => opt.MapFrom((rolefaculty, response) => rolefaculty.Faculty.Name))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<Role, AuthorityRoleResponse>()
                .ForMember(dest => dest.Abbreviation, opt => opt.MapFrom(src => src.Abbreviation))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Faculties, opt => opt.MapFrom(src => src.Rolefaculties))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<Userrole, AuthorityResponse>()
                .ForMember(dest => dest.Abbreviation,
                    opt => opt.MapFrom((userrole, response) => userrole.Role.Abbreviation))
                .ForMember(dest => dest.Name, opt => opt.MapFrom((userrole, response) => userrole.Role.Name))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<User, UserResponse>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(src => src.Creationdate))
                .ForMember(dest => dest.ModificationDate, opt => opt.MapFrom(src => src.Modificationdate))
                .ForMember(dest => dest.Authorities, opt => opt.MapFrom(src => src.Userroles))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<UpdatePasswordRequest, User>()
                .ForMember(dest => dest.Pwd, opt => opt.MapFrom(src => src.Pwd))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<UpdateUserRequest, UserPatch>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForAllOtherMembers(x => x.Ignore());

            this.CreateMap<AddUserRequest, User>()
                .BeforeMap((request, user) =>
                {
                    user.Creationdate = DateTime.Now;
                    user.Modificationdate = null;
                    user.Userroles = new List<Userrole>();
                    foreach (var roleItem in request.Roles)
                    {
                        user.Userroles.Add(new Userrole
                        {
                            Role = new Role
                            {
                                Abbreviation = roleItem
                            },
                            User = user
                        });
                    }
                })
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.Pwd, opt => opt.MapFrom(src => src.Pwd))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForAllOtherMembers(x => x.Ignore());
        }
    }
}