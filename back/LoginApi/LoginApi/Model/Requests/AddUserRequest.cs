// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AddUserRequest.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using A2net.WebHelpers.DataAnnotations;
    using Annotations;

    /// <summary>
    /// Add user request.
    /// </summary>
    [PasswordValidation]
    public sealed class AddUserRequest
    {
        /// <summary>
        /// Email.
        /// </summary>
        [Email(ErrorMessage = ValidationValues.EmailFormat)]
        public string Email { get; set; }

        /// <summary>
        /// Username (length of 7)
        /// </summary>
        [Required(ErrorMessage = ValidationValues.UsernameRequired)]
        [MinLength(ValidationValues.UsernameMinLengthValue, ErrorMessage = ValidationValues.UsernameMinLength)]
        public string Username { get; set; }

        /// <summary>
        /// Gender (F)emale or (M)ale
        /// </summary>
        [Group(new[] {ValidationValues.GenderF, ValidationValues.GenderM}, IsCaseSensitive = false,
            ErrorMessage = ValidationValues.GenderRequired)]
        public string Gender { get; set; }

        /// <summary>
        /// User status, <code>true</code> is enabled, otherwise disabled.
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Password in Base64 format.
        /// </summary>
        public string Pwd { get; set; }

        /// <summary>
        /// Password in base64 format.
        /// </summary>
        public string PwdConfirmation { get; set; }

        /// <summary>
        /// Array of roles for this user.
        /// </summary>
        [MinLength(1, ErrorMessage = ValidationValues.RolesRequired)]
        public string[] Roles { get; set; }
    }
}