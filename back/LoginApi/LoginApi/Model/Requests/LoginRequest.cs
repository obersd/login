// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginRequest.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using Annotations;

    /// <summary>
    /// Request a user login. 
    /// </summary>
    public sealed class LoginRequest
    {
        /// <summary>
        /// User name.
        /// </summary>
        [Required(ErrorMessage = ValidationValues.UsernameRequired)]
        [MinLength(ValidationValues.UsernameMinLengthValue, ErrorMessage = ValidationValues.UsernameMinLength)]
        public string Username { get; set; }

        /// <summary>
        /// Password in base64 format.
        /// </summary>
        [Required(ErrorMessage = ValidationValues.PasswordRequired)]
        [Base64Validation(ErrorMessage = ValidationValues.Base64Format)]
        [MinLength(ValidationValues.PasswordMinLengthValue, ErrorMessage = ValidationValues.PasswordMinLength)]
        public string Pwd { get; set; }
    }
}