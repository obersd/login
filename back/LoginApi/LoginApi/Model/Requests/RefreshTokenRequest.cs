namespace LoginApi.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using Annotations;

    /// <summary>
    /// Request a new access token.
    /// </summary>
    public sealed class RefreshTokenRequest
    {
        /// <summary>
        /// Refresh token. 
        /// </summary>
        [Required(ErrorMessage = ValidationValues.TokenRequired)]
        public string Token { get; set; }
    }
}