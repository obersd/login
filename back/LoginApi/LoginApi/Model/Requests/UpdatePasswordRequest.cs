// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdatePasswordRequest.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using Annotations;

    /// <summary>
    /// Update password request.
    /// </summary>
    [PasswordValidation]
    public sealed class UpdatePasswordRequest
    {
        /// <summary>
        /// User id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Password in base64 format.
        /// </summary>
        public string Pwd { get; set; }

        /// <summary>
        /// Password in base64 format.
        /// </summary>
        public string PwdConfirmation { get; set; }
    }
}