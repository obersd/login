namespace LoginApi.Model.Requests
{
    using Annotations;
    using Entities.Model;

    /// <summary>
    /// Update user request.
    /// </summary>
    public sealed class UpdateUserRequest
    {
        /// <summary>
        /// User id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Username.
        /// </summary>
        [UserCustomValidation(UserCustomValidationAttribute.DataType.String,
            ValidationValues.UsernameMinLengthValue,
            ErrorMessage = ValidationValues.UsernameMinLength)]
        public Patch<string> Username { get; set; }

        /// <summary>
        /// Email.
        /// </summary>
        [UserCustomValidation(UserCustomValidationAttribute.DataType.Email,
            0, ErrorMessage = ValidationValues.EmailFormat)]
        public Patch<string> Email { get; set; }

        /// <summary>
        /// Gender (F)emale or (M)ale.
        /// </summary>
        [UserCustomValidation(UserCustomValidationAttribute.DataType.Gender,
            0,
            new[] {ValidationValues.GenderF, ValidationValues.GenderM},
            ErrorMessage = ValidationValues.GenderRequired)]
        public Patch<string> Gender { get; set; }
    }
}