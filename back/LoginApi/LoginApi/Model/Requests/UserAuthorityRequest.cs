// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserAuthorityRequest.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Requests
{
    using System.ComponentModel.DataAnnotations;
    using System.Text.Json.Serialization;
    using Annotations;

    /// <summary>
    /// Adds or removes user authority request.
    /// </summary>
    public sealed class UserAuthorityRequest
    {
        /// <summary>
        /// User id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Authority abbreviation.
        /// </summary>
        [Required(ErrorMessage = ValidationValues.AuthAbbreviation)]
        [JsonPropertyName(("abbr"))]
        public string Abbreviation { get; set; }
    }
}