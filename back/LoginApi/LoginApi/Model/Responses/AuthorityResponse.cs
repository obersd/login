namespace LoginApi.Model.Responses
{
    using System.Text.Json.Serialization;

    /// <summary>
    /// Authorities response.
    /// </summary>
    public class AuthorityResponse
    {
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Abbreviation.
        /// </summary>
        [JsonPropertyName(("abbr"))]
        public string Abbreviation { get; set; }
    }
}