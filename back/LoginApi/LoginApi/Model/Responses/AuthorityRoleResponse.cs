namespace LoginApi.Model.Responses
{
    using System.Collections.Generic;

    /// <summary>
    /// Authority with faculties response.
    /// </summary>
    public sealed class AuthorityRoleResponse : AuthorityResponse
    {
        /// <summary>
        /// Role Faculties.
        /// </summary>
        public IList<FacultyResponse> Faculties { get; set; }
    }
}