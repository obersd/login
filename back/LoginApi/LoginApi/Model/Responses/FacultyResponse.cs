namespace LoginApi.Model.Responses
{
    /// <summary>
    /// Faculty response.
    /// </summary>
    public sealed class FacultyResponse
    {
        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description.
        /// </summary>
        public string Description { get; set; }
    }
}