// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LoginResponse.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Responses
{
    /// <summary>
    /// Response when login is OK.
    /// </summary>
    public sealed class LoginResponse
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="token">Token.</param>
        public LoginResponse(string token)
        {
            this.Token = token;
        }

        /// <summary>
        /// JWT token.
        /// </summary>
        public string Token { get; }
    }
}