// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserResponse.cs" company="lex vargas">
//   lex vargas ® México 2021
//   El código fuente y todos los componentes y archivos de éste 
//   software son propiedad intelectual de lex vargas ©.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace LoginApi.Model.Responses
{
    using System;
    using System.Collections.Generic;
    using System.Text.Json.Serialization;

    /// <summary>
    /// User response.
    /// </summary>
    public sealed class UserResponse
    {
        /// <summary>
        /// User id.
        /// </summary>
        public int UserId { get; set; }
        
        /// <summary>
        /// Username.
        /// </summary>
        public string Username { get; set; }
        
        /// <summary>
        /// Email.
        /// </summary>
        public string Email { get; set; }
        
        /// <summary>
        /// Gender F or M 
        /// </summary>
        public string Gender { get; set; }
        
        /// <summary>
        /// Status.
        /// </summary>
        public bool Status { get; set; }
        
        /// <summary>
        /// Creation date.
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Modification date.
        /// </summary>
        public DateTime? ModificationDate { get; set; }

        /// <summary>
        /// User authorities.
        /// </summary>
        public IList<AuthorityResponse> Authorities { get; set; }
    }
}