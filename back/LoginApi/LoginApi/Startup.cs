using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace LoginApi
{
    using A2net.Swagger.SwaggerMiddleware;
    using A2net.WebHelpers.Responses;
    using Entities.Model.Security;
    using Infrastructure;

    /// <summary>
    /// Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Ctor.
        /// </summary>
        /// <param name="configuration">Configuration object.</param>
        public Startup(IConfiguration configuration) => this.Configuration = configuration;

        /// <summary>
        /// Configuration object.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Service collection.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().ConfigureApiBehaviorOptions(
                options =>
                    options.InvalidModelStateResponseFactory = context =>
                        ActionResultFactory.InvalidModelResponse(context.ModelState, () => "Error de validación")
            );

            services.AddApiServices(this.Configuration);
            services.AddCors(options =>
            {
                options.AddPolicy(Policies.CorsAllowOrigins,
                    builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod(); });
            });
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">App.</param>
        /// <param name="env">Host Environment</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerAndUi("Login API V1");
            }

            app.UseHsts();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(Policies.CorsAllowOrigins);
            app.UseAuthentication();
            app.UseAuthorization();
            // app.UseMiddleware<TokenRefreshMiddleware>();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}