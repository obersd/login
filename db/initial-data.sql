-- Este script permite cargar datos iniciales en la base de datos LoginDb.
-- Se asume una configuración de semillas inicializada en 1.

-- Usuarios. La contraseña para todos los usuarios es 'Aa.12345'
insert into users (username, email, gender, status, pwd, creationdate, modificationdate) values ('alex.vargas','alex@yopmail.com','M',1,'$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq',GETDATE(),null);
insert into users (username, email, gender, status, pwd, creationdate, modificationdate) values ('ruben.vargas','ruben@yopmail.com','M',1,'$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq',GETDATE(),null);
insert into users (username, email, gender, status, pwd, creationdate, modificationdate) values ('nancy.vargas','nan@yopmail.com','F',0,'$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq',GETDATE(),null);
insert into users (username, email, gender, status, pwd, creationdate, modificationdate) values ('user.master','user@yopmail.com','M',1,'$2b$10$PJ/vFPF3bsaSmqp7Jm4luOQ56ihMfi.2Vq4R8.P4mQjGFA.UO37Lq',GETDATE(),null);

-- Roles
insert into roles (name, abbreviation) values ('MANAGER','ROOT');
insert into roles (name, abbreviation) values ('USER','UID');
insert into roles (name, abbreviation) values ('SUPERVISOR','SVS');

-- Facultades
insert into faculties (name, abbreviation, description) values ('REGISTRO DE USUARIOS','ADDUSR','Permite crear nuevos usuarios.');
insert into faculties (name, abbreviation, description) values ('ACTUALIZAR DATOS', 'CINFO', N'Permite actualizar su información.');
insert into faculties (name, abbreviation, description) values (N'CAMBIAR AUTORIZACIÓN', 'CAUTH', 'Permite modificar roles de usuario.');

-- Roles y sus facultades
insert into rolefaculties (roleId, facultyId) values (1, 1);
insert into rolefaculties (roleId, facultyId) values (1, 2);
insert into rolefaculties (roleId, facultyId) values (1, 3);
insert into rolefaculties (roleId, facultyId) values (2, 2);
insert into rolefaculties (roleId, facultyId) values (3, 1);
insert into rolefaculties (roleId, facultyId) values (3, 2);

-- Relación de usuarios y roles
-- alex.vargas -> MANAGER
insert into userroles (userId, roleId) values (1,1);

-- ruben.vargas -> SUPERVISOR
insert into userroles (userId, roleId) values (2,3);

-- nancy.vargas -> SUPERVISOR
insert into userroles (userId, roleId) values (3,3);

-- user.master -> USER
insert into userroles (userId, roleId) values (4,2);
