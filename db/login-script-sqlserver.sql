-- Este script genera una base de datos llamada LoginDb y genera las estructuras necesarias para almacenar información
-- de un inicio de sesión simple.

-- Es importante recordar que esta instrucción es asíncrona, por lo que el flujo de ejecución seguirá aún cuando la
-- base de datos no se ha creado. Esta instrucción debe ejecutarse de manera independiente.
-- create database LoginDb;

-- Inicio del script de inicialización de objetos
use LoginDb;

drop table if exists refreshtokens;
drop table if exists userroles;
drop table if exists rolefaculties;
drop table if exists faculties;
drop table if exists users;
drop table if exists roles;

-- Representa y almacena las propiedades mínimas de un usuario.
create table users(
    userId              integer identity not null primary key,  -- Llave primaria
    username            nvarchar(32) not null,                  -- Nombre de usuario
    email               nvarchar(64) not null,                  -- Correo electŕonico
    gender              nchar(1) not null,                      -- Género o sexo M o F
    status              bit not null,                           -- Especifica si está activo o inactivo-
    pwd                 nvarchar(512) not null,                 -- Password computado con un algoritmo Hash.
    creationdate        datetime not null,                      -- Fecha de creación.
    modificationdate    datetime null                           -- Fecha de modificación.
);

-- Representa y almacena las propiedades mínimas de un rol.
create table roles(
    roleId          integer identity not null primary key,  -- Llave primaria.
    name            nvarchar(128) not null,                 -- Nombre del rol.
    abbreviation    nvarchar(16) not null                   -- Abreviatura.
);

-- Representa y almacena la relación entre un usuario y los roles asignados a él.
create table userroles(
    userId integer not null constraint userroles_userid_fk references users(userId),    -- Llave foránea.
    roleId integer not null constraint userroles_roleid_fk references roles(roleId),    -- LLave foránea.
    primary key (userId, roleId)                                                        -- Llave primaria.
);

-- Representa y almacena los token de refresco que serán usados para un intercambio por un token de acceso reciente.
create table refreshtokens(
    userId          integer not null constraint refreshtokens_userid_fk references users(userId),   -- Llave foránea.
    token           nvarchar(128) not null,                                                         -- Token de refresco.
    creationdate    datetime not null,                                                              -- Fecha y hora de creación.
    isactive        bit not null,                                                                   -- Especifica si el token está activo.
    primary key (userId, token)                                                                     -- Llave primaria.
);

-- Representa y almacena las facultades que un rol tiene asignadas.
create table faculties(
    facultyId       integer identity not null primary key,  -- Llave primaria.
    name            nvarchar(128) not null,                 -- Nombre de la facultad.
    description     nvarchar(512) not null,                 -- Descripción de la facultad.
    abbreviation    nvarchar(16) not null                   -- Abreviatura.
)

-- Representa y almacena la relación entre un rol y sus facultades asignadas.
create table rolefaculties(
    roleId      integer not null constraint rolefaculties_roleId_fk references roles(roleId),               -- Llave foránea.
    facultyId   integer not null constraint rolefaculties_facultyId_fk references faculties(facultyId),     -- Llave foránea.
    primary key (roleId, facultyId)                                                                         -- Llave primaria.
);
