import { HTTP_INTERCEPTORS, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  HttpInterceptor, HttpHandler,
  HttpRequest, HttpErrorResponse
} from '@angular/common/http';

import { TokenStorageService } from '../_services/token-storage.service';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, filter, take, switchMap, finalize } from "rxjs/operators";
import { AuthService } from '../_services/auth.service';
import { UserIdentity } from '../_models/user-identity';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private token: TokenStorageService, private authService: AuthService) { }
  private updatingTheToken = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Add this header always...
    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        headers: req.headers.set('Content-Type', 'application/json')
      });
    }

    // Try extract acces token and adds to request...
    req = this.addAuthenticationToken(req);
    return next.handle(req).pipe(
      catchError((error: HttpErrorResponse) => {
        // Error with Httpstatus has occured. We need to refresh access token. 
        if (error && error.status === 401) {
          if (this.updatingTheToken) {
            // Other requests will wait until refreshtokenSubject has a new access token. 
            // This means the requests is ready to retry with the new access token.
            return this.refreshTokenSubject.pipe(
              filter(result => result !== null),
              take(1),
              switchMap(() => next.handle(this.addAuthenticationToken(req)))
            );
          } else {
            this.updatingTheToken = true;
            this.refreshTokenSubject.next(null);
            // Invoke endpoint to refresh access token
            return this.refreshAccessToken().pipe(
              switchMap((success: boolean) => {
                this.refreshTokenSubject.next(success);
                return next.handle(this.addAuthenticationToken(req));
              }),
              // When the call to refreshToken completes, reset the refreshTokenInProgress to false
              finalize(() => this.updatingTheToken = false)
            );
          }
        } else {
          return throwError(error);
        }
      })
    );
  }

  /**
   * Invokes refresh token endpoint and stores in localStorage.
   * Returns the new access token.
   */
  private refreshAccessToken(): Observable<any> {
    this.authService.refreshAccessToken().subscribe(data => {
      const subject = JSON.parse(atob(data.token.split('.')[1]));
      const user = UserIdentity.toUser(JSON.stringify(subject.sub));
      this.token.saveToken(data.token);
      this.token.saveUser(user);
    });

    return of(this.token.getToken());
  }

  /**
   * Adds the current access token to a request.
   * @param req Current request.
   */
  private addAuthenticationToken(req: HttpRequest<any>): HttpRequest<any> {
    if (req.url.match(/token/) || req.url.match(/api\/auth\/token/)) {
      return req;
    }

    let authReq = req;
    const token = this.token.getToken();
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token) });
    }
    return authReq;
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
];
