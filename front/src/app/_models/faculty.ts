export interface Faculty {
    name: string;
    description: string;
}
