import { Role } from './role';

export interface IUserIdentity {
    id: number;
    name: string;
    mail: string;
    roles: Role[];
}