export interface IUser {
    email: string,
    username: string,
    gender: string,
    status: boolean,
    pwd: string,
    pwdConfirmation: string,
    roles: string[]
    creationDate: Date | null;
    modificationDate: Date | null;
}