import { Faculty } from './faculty';
export class Role {
    constructor(public name: string,
        public abbr: string,
        public faculties: Faculty[]) { }

    public static create(): Role {
        return new Role("", "", []);
    }
}