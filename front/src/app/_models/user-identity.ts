import { Role } from './role';
import { IUserIdentity } from './i-user-identity';

export class UserIdentity implements IUserIdentity {
    constructor(public id: number, public name: string, public mail: string, public roles: Role[]) { }

    public static isUserInRole(roles: Role[], roleAbbr: string) {
        if (roles.length == 0) {
            return false;
        }

        for (const iterator of roles) {
            if (iterator.abbr == roleAbbr) {
                return true;
            }
        }

        return false;
    }

    public static toUser(json: string): UserIdentity {
        return JSON.parse(json);
    }

    public static isInRole(userIdentity: UserIdentity, roleAbbr: string): boolean {
        if (userIdentity.roles.length == 0) {
            return false;
        }

        for (const iterator of userIdentity.roles) {
            if (iterator.abbr == roleAbbr) {
                return true;
            }
        }

        return false;
    }
}