import { IUser } from './i-user';
import { Role } from './role';
import { UserIdentity } from './user-identity';
export class User implements IUser {
    constructor(
        public userId: number = 0,
        public email: string = "",
        public username: string = "",
        public gender: string = "",
        public status: boolean = true,
        public roles: string[] = [],
        public pwd: string = "",
        public pwdConfirmation: string = "",
        public creationDate: Date | null = null,
        public modificationDate: Date | null = null,
        public authorities: Role[] = []) {
        this.pwd = btoa(pwd);
        this.pwdConfirmation = btoa(pwdConfirmation);
    }

    public static create(): User {
        return new User();
    }

    public static convert(identity: UserIdentity): User {
        return new User(identity.id, identity.mail, identity.name, "", true, [], "", "", null, null,[]);
    }
}