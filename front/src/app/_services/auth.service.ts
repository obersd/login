import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { Token } from './../_models/token';
import { TokenStorageService } from './token-storage.service';
import { Router } from '@angular/router';

/**
 * Authentication service.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private tokenStorageService: TokenStorageService, private router: Router) { }

  /**
   * Login in the system.
   * @param username Username or email.
   * @param password User password.
   */
  login(username: string, password: string): Observable<Token> {
    const pwd = btoa(password);
    return this.http.post<Token>(environment.endpoint + 'auth/login', {
      username,
      pwd
    });
  }

  /**
   * Refresh the access token.
   */
  refreshAccessToken(): Observable<Token> {
    return this.http.post<Token>(environment.endpoint + "auth/token", {
      token: this.tokenStorageService.getToken()
    });
  }
}
