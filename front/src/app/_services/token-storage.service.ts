import { Injectable } from '@angular/core';
import { UserIdentity } from '../_models/user-identity';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  constructor() { }

  /**
   * Clears session storage.
   */
  signOut(): void {
    window.sessionStorage.clear();
  }

  /**
   * Saves the token to local storage.
   */
  public saveTokenToLocal(token: string): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  }

  /**
   * Gest the token from local storage.
   */
  public getTokenFromLocal(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }

  /**
   * Clears all storages.
   */
  public clearAll(): void {
    this.signOut();
    this.clearLocalToken();
  }

  /**
   * Clear local storage.
   */
  public clearLocalToken(): void {
    window.localStorage.clear();
  }

  public addValue(name: string, value: string): void {
    window.sessionStorage.setItem(name, value);
  }

  public getValue(name: string): string | null {
    return window.sessionStorage.getItem(name);
  }

  public removeValue(name: string): void {
    window.sessionStorage.removeItem(name);
  }

  /**
   * Saves token.
   */
  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  /**
   * Gets token.
   */
  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  /**
   * Saves an user.
   */
  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  /**
   * Gets an user.
   */
  public getUser(): UserIdentity | null {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return null;
  }
}
