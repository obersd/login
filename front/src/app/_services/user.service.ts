import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { User } from '../_models/user';
import { Role } from '../_models/role';

/**
 * User service.
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) { }

  /**
   * Searchs users.
   * @param status User status.
   * @param term Search terms.
   */
  searchUsers(status: boolean, term: string): Observable<User[]> {
    return this.http.get<User[]>(environment.endpoint + "users/user?st=" + status + "&w=" + term);
  }

  /**
   * Change user status.
   * @param userId  user id.
   * @param status status (false=disabled) (true=enabled)
   */
  changeUserStatus(userId: number, status: boolean): Observable<any> {
    return this.http.put(environment.endpoint + "users/user/" + userId + "/" + status, {});
  }

  /**
   * Gets user data.
   * @param userId user id.
   */
  getUser(userId: number): Observable<User> {
    return this.http.get<User>(environment.endpoint + "users/user/" + userId);
  }

  /**
   * Gets all roles.
   */
  getAllRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(environment.endpoint + "users/roles");
  }

  /**
   * Adds role to user.
   * @param userId user id.
   * @param roleAbbr role abbreviation.
   */
  addRole(userId: number, roleAbbr: string): Observable<any> {
    return this.http.post(environment.endpoint + "users/roles", {
      userId: userId,
      abbr: roleAbbr
    });
  }

  /**
   * Removes role from user.
   * @param userId user id.
   * @param roleAbbr role abbreviation.
   */
  removeRole(userId: number, roleAbbr: string): Observable<any> {
    return this.http.delete(environment.endpoint + "users/roles?uid=" + userId + "&abbr=" + roleAbbr);
  }

  /**
   * Updates password.
   * @param userId user id.
   * @param pwd password.
   * @param pwdConfirmation passwordconfirmation.
   */
  updatePassword(userId: number, pwd: string, pwdConfirmation: string): Observable<any> {
    return this.http.put(environment.endpoint + "users/user", {
      userId,
      pwd: btoa(pwd),
      pwdConfirmation: btoa(pwdConfirmation)
    })
  }

  /**
   * Adds a user to the system.
   * @param user User.
   */
  register(user: User): Observable<any> {
    return this.http.post(environment.endpoint + 'users/user', user);
  }

  /**
   * Updates/patches the user data.
   * @param userId user id.
   * @param changeUsername Specifies whether the username will be updated.
   * @param username username.
   * @param changeGender Specifies whether the gender will be updated.
   * @param gender gender.
   * @param changeEmail Specifies whether the email will be updated.
   * @param email email.
   */
  updateUser(userId: number, changeUsername: boolean, username: string, changeGender: boolean, gender: string,
    changeEmail: boolean, email: string): Observable<any> {
    return this.http.patch(environment.endpoint + "users/user", {
      userId,
      username: changeUsername ? { data: username } : null,
      email: changeEmail ? { data: email } : null,
      gender: changeGender ? { data: gender } : null
    });
  }
}