import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './_services/token-storage.service';
import { User } from './_models/user';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserDataComponent } from './user-data/user-data.component';
import { environment } from './../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[] = [];
  public isLoggedIn = false;
  public username?: string;
  public isManager: boolean = false;
  public isUser: boolean = false;
  public isSupervisor: boolean = false;

  constructor(private tokenStorageService: TokenStorageService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      if (user != null) {
        let rolesArray: string[] = [];
        user.roles.map(function (value) {
          rolesArray.push(value.abbr);
        });
        this.roles = rolesArray;
        this.isManager = this.roles.includes(environment.Roles.Manager);
        this.isSupervisor = this.roles.includes(environment.Roles.Supervisor);
        this.isUser = this.roles.includes(environment.Roles.User);
        this.username = user.name;
      }
    }
  }

  showProfile() {
    const identityUser = this.tokenStorageService.getUser();
    if (identityUser != null) {
      const modalRef = this.modalService.open(UserDataComponent)
      modalRef.componentInstance.userId = identityUser.id;
    }
  }

  logout(): void {
    this.tokenStorageService.addValue("SESSION_FLAG", "1");
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
