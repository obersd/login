import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from './../_services/token-storage.service';

/**
 * This components controlls if an user is logged in. If not, then redirect him to login.
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router, private storage: TokenStorageService) { }
  ngOnInit(): void {
    if (!!this.storage.getToken() == false) {
      this.router.navigate(['login']);
    }
  }
}
