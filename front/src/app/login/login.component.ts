import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { Router } from '@angular/router';
import { UserIdentity } from '../_models/user-identity';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null,
    rememberme: false
  };
  errorMessage: string = "";
  isLoginFailed: boolean = false;
  showContinueAs: boolean = false;

  constructor(private authService: AuthService, private router: Router, private tokenStorageService: TokenStorageService) { }

  /**
   * Creates the user from the local token.
   */
  onContinueAs(): void {
    var localStorageToken = this.tokenStorageService.getTokenFromLocal();
    if (localStorageToken != null) {
      this.convertToUserAndSave(localStorageToken, true);
      window.location.reload();
    }
    else {
      this.onDeleteAllAndShowLogin();
    }
  }

  /**
   * Removes token from local storage and show Login UI.
   */
  onDeleteAllAndShowLogin(): void {
    this.showContinueAs = false;
    this.tokenStorageService.clearLocalToken();
    this.tokenStorageService.signOut();
  }

  /**
   * Component initializing. 
   * - If exists local and session token, redirects to home.
   * - If exists local but not exists session token, show "Continue as" UI.
   * - If not exists local but eists session token, redirects to home.
   * - Otherwhise do nothing.
   */
  ngOnInit(): void {
    this.errorMessage = "";
    var localStorageToken = this.tokenStorageService.getTokenFromLocal();
    var sessionStorageToken = this.tokenStorageService.getToken();

    if (localStorageToken != null && sessionStorageToken != null) {
      this.router.navigate(['home']);
    }
    else if (localStorageToken != null && sessionStorageToken == null) {
      var flag = this.tokenStorageService.getValue("SESSION_FLAG");
      if (flag != null) {
        this.tokenStorageService.removeValue("SESSION_FLAG");
        this.convertToUserAndSave(localStorageToken, true);
        window.location.reload();
      }
      else {
        this.showContinueAs = true;
      }
    }
    else if (localStorageToken == null && sessionStorageToken != null) {
      this.router.navigate(['home']);
    }
    else if (localStorageToken == null && sessionStorage == null) {
      // Do nothing...
    }
  }

  /**
   * Creates a user and save it into local storage.
   * @param token Access token.
   * @param rememberme Specifies whether the token is remembered.
   */
  convertToUserAndSave(token: string, rememberme: boolean) {
    const subject = JSON.parse(atob(token.split('.')[1]));
    const user = UserIdentity.toUser(JSON.stringify(subject.sub));
    this.tokenStorageService.saveToken(token);
    this.tokenStorageService.saveUser(user);
    if (rememberme) {
      this.tokenStorageService.saveTokenToLocal(token);
    }
  }

  /**
   * Requests to user login and redirects to home.
   */
  onSubmit(): void {
    const { username, password } = this.form;
    this.authService.login(username, password).subscribe(
      data => {
        this.convertToUserAndSave(data.token, this.form.rememberme);
        window.location.reload();
      },
      err => {
        this.isLoginFailed = true;
        this.errorMessage = "Credenciales inválidas.";
      }
    );
  }
}
