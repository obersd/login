import { Component, OnInit } from '@angular/core';
import { environment } from './../../environments/environment';
import { User } from '../_models/user'
import { Role } from '../_models/role'
import { UserService } from '../_services/user.service';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';

/**
 * This component registers new users into the system.
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {
    username: null,
    email: null,
    password: null,
    passwordConfirmation: null,
    sexo: null,
    isActive: true,
    role: null
  };
  roles: Role[] = [];
  selectedRole: Role = Role.create();
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  sexos = environment.Sexo;
  passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/;
  public isErrorConfirmation = false;
  public isErrorRegex = false;

  constructor(private userService: UserService, private router: Router, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.userService.getAllRoles().subscribe(data => {
      this.roles = data;
    },
      err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login']);
        }
      });
  }
  onRoleChange(e: any) {
    var roles = this.roles.filter(x => x.abbr == this.form.role);
    if (roles.length > 0) {
      this.selectedRole = roles[0];
    }
  }

  onPasswordChange() {
    this.isErrorConfirmation = this.isErrorRegex = false;
  }

  onSubmit() {
    if (this.form.password != this.form.passwordConfirmation) {
      this.isErrorConfirmation = true;
    }
    else if (!this.passwordRegex.test(this.form.password)) {
      this.isErrorRegex = true;
    }
    else {
      const user = new User(0, this.form.email, this.form.username, this.form.sexo, this.form.isActive,
        [this.form.role], this.form.password, this.form.passwordConfirmation, null, null);

      this.userService.register(user).subscribe(
        data => {
          this.isSuccessful = true;
          this.isSignUpFailed = false;
        }, err => {
          if (err.status === 401) {
            this.tokenStorageService.clearAll();
            this.router.navigate(['login']);
          }
          else {
            this.errorMessage = err.error.message;
            this.isSignUpFailed = true;
            if (!environment.production) {
              console.error(err);
            }
          }
        });
    }
  }
}
