import { Component, OnInit, Input } from '@angular/core';
import { User } from '../_models/user';
import { UserService } from './../_services/user.service';
import { ExportService } from './../_services/export.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserDataComponent } from '../user-data/user-data.component';
import { Router } from '@angular/router';
import { TokenStorageService } from '../_services/token-storage.service';

/**
 * This component performs users searchs. Permits change user status and edit user data.
 */
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  constructor(private userService: UserService, private router: Router, private tokenStorageService: TokenStorageService, private exportService: ExportService, private modalService: NgbModal) { }
  public users: User[] = [];
  public searchActiveUsers = true;
  public terms: string = "";

  ngOnInit() {
  }

  /**
   * Shows modal with user data.
   * @param item user data.
   */
  onEditUser(item: any) {
    const modalRef = this.modalService.open(UserDataComponent)
    modalRef.componentInstance.userId = item.userId;
  }

  /**
   * Change user status.
   * @param a user data.
   */
  onDeleteUser(a: User) {
    this.userService.changeUserStatus(a.userId, !a.status).subscribe(
      () => {
        this.onSearch();
      },
      err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login'])
        }
      }
    );
  }

  /**
   * Export table to an excel file.
   */
  onExport() {
    this.exportService.exportExcel(this.users, 'usuarios');
  }

  onSearch() {
    this.userService.searchUsers(this.searchActiveUsers, this.terms).subscribe(
      data => {
        this.users = data;
      },
      err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login'])
        }
      }
    )
  }
}
