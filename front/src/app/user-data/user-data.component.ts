import { Component, OnInit, Input } from '@angular/core';
import { User } from './../_models/user';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../_services/user.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { environment } from 'src/environments/environment';
import { UserIdentity } from '../_models/user-identity';
import { Role } from '../_models/role';
import { Router } from '@angular/router';
import { AlertService } from '../_services/alert.service';

/**
 * This component edits a user data.
 */
@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.css']
})
export class UserDataComponent implements OnInit {
  @Input() userId: number = 0;

  public loadingMessage: string = "Cargando...";
  public user: User = User.create();
  public showAuthorizationRoles = false;
  public errorMessage = '';
  public sexos = environment.Sexo;
  public isErrorConfirmation = false;
  public isErrorRegex = false;
  public userRoles: Role[] = [];
  public availableRoles: Role[] = [];

  roles: Role[] = [];
  passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/;
  form: any = {
    username: null,
    email: null,
    password: null,
    passwordConfirmation: null,
    sexo: null
  };

  constructor(public activeModal: NgbActiveModal, private userService: UserService, private tokenStorageService: TokenStorageService, private router: Router,
    private alertService: AlertService) { }
  ngOnInit() {
    this.loadUserUi();
  }

  loadUserUi() {
    this.userService.getUser(this.userId).subscribe(
      data => {
        this.user = data;
        this.form.username = data.username;
        this.form.email = data.email;
        this.form.sexo = data.gender;
        this.loadingMessage = this.user.username;
        const userIdentity = this.tokenStorageService.getUser();
        if (userIdentity != null) {
          this.showAuthorizationRoles = UserIdentity.isInRole(userIdentity, environment.Roles.Manager);
          this.loadRoleUi();
        }
        else {
          this.showAuthorizationRoles = false;
        }
      }, err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login']);
        }
      });
  }

  loadRoleUi() {
    this.userService.getAllRoles().subscribe(
      data => {
        this.roles = data;
        this.availableRoles = [];
        this.userRoles = [];
        let tmpDisponibles = this.availableRoles;
        let tmpNoDisponibles = this.userRoles;
        let rolUsuario = this.user.authorities;
        this.roles.forEach(function (item) {
          var roles = rolUsuario.filter(x => x.abbr == item.abbr);
          if (roles.length > 0) {
            tmpNoDisponibles.push(roles[0]);
          }
          else {
            tmpDisponibles.push(item);
          }
        });

        this.availableRoles = tmpDisponibles;
        this.userRoles = tmpNoDisponibles;
      }, err => { }
    );
  }

  onPasswordChange() {
    this.isErrorConfirmation = this.isErrorRegex = false;
  }

  onSubmitPasswords() {
    if (this.form.password != this.form.passwordConfirmation) {
      this.isErrorConfirmation = true;
    }
    else if (!this.passwordRegex.test(this.form.password)) {
      this.isErrorRegex = true;
    }
    else {
      this.userService.updatePassword(this.userId, this.form.password, this.form.passwordConfirmation).subscribe(
        data => {
          this.form.password = this.form.passwordConfirmation = "";
          this.alertService.alert("Contraseña cambiada correctamente.");
        },
        err => {
          if (err.status === 401) {
            this.tokenStorageService.clearAll();
            this.router.navigate(['login']);
          }
        });
    }
  }

  onSubmit() {
    const changeUsername = this.form.username == this.user.username ? false : true;
    const changeGender = this.form.sexo == this.user.gender ? false : true;
    const changeEmail = this.form.email == this.user.email ? false : true;
    this.userService.updateUser(this.userId, changeUsername, changeUsername ? this.form.username : "",
      changeGender, changeGender ?
      this.form.sexo : "", changeEmail, changeEmail ? this.form.email : "").subscribe(
        () => {
          this.loadUserUi();
          this.alertService.alert("Datos actualizados correctamente.");
        }, err => {
          if (err.status === 401) {
            this.tokenStorageService.clearAll();
            this.router.navigate(['login']);
          }
          else {
            this.alertService.alert(err.error.errors[0]);
          }
        });
  }

  onAddRole(r: any) {
    this.userService.addRole(this.userId, r.abbr).subscribe(data => {
      this.loadUserUi();
      this.alertService.alert("Rol agregado.");
    },
      err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login']);
        }
      });
  }

  onRemoveRole(r: any) {
    this.userService.removeRole(this.userId, r.abbr).subscribe(data => {
      this.loadUserUi();
      this.alertService.alert("Rol eliminado.");
    },
      err => {
        if (err.status === 401) {
          this.tokenStorageService.clearAll();
          this.router.navigate(['login']);
        }
      });
  }
}
