export const environment = {
  production: true,
  endpoint:"https://localhost:5001/api/",
  headerAppJson: { 'Content-Type': 'application/json' },
  Roles: {
    Manager: 'ROOT',
    User: 'UID',
    Supervisor: 'SUPERVISOR'
  },
  Sexo: [{ abbr: 'F', name: 'FEMENINO' }, { abbr: 'M', name: 'MASCULINO' }]
};
